-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Creato il: Ott 24, 2016 alle 11:06
-- Versione del server: 10.1.13-MariaDB
-- Versione PHP: 7.0.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `cda-spa`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `cms_carrello`
--

CREATE TABLE `cms_carrello` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_cliente` varchar(255) NOT NULL,
  `id_prodotto` varchar(255) NOT NULL,
  `quantita` int(11) NOT NULL,
  `prezzo` double NOT NULL,
  `deposito` varchar(5) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indici per le tabelle scaricate
--

--
-- Indici per le tabelle `cms_carrello`
--
ALTER TABLE `cms_carrello`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT per le tabelle scaricate
--

--
-- AUTO_INCREMENT per la tabella `cms_carrello`
--
ALTER TABLE `cms_carrello`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
