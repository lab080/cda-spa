<?php
  error_reporting(E_ALL);
  ini_set('display_startup_errors', 1);
  ini_set('display_errors', 1);

  $dom = new DomDocument('1.0','UTF-8');
  function writeFile($name, $json , $offset = false) {
    $datenow = date("YmdHis");
    // $fh = fopen('../file_update/'.$name.'_'.$datenow, 'w');
    // fwrite($fh, json_encode($json));
    // fclose($fh);

    $dom = new DOMDocument('1.0','UTF-8');
    $dom->preserveWhiteSpace = false;
    $dom->formatOutput = true;
    $dom_data = $dom->appendChild(new DOMElement('data'));
    array_to_xml($json,$dom_data);

    $file_name = $name.'_'.$datenow;
    if($offset) { $file_name .= '-'.$offset; }

    $dom->save('../file_update/'.$file_name.".xml");
  }

  function array_to_xml( $data, $dom ) {
    foreach( $data as $key => $value ) {
        $key = strtolower($key);
        if( is_array($value) ) {
            if( is_numeric($key) ){
                $key = 'item'; //dealing with <0/>..<n/> issues
            }
            $key = strtolower($key);
            $subnode = $dom->appendChild(new DOMElement($key));
            array_to_xml($value, $subnode);
        } else {
            $value = preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $value);
            $value = utf8_encode(htmlspecialchars($value));
            $dom->appendChild(new DOMElement($key))->appendChild(new DOMCdataSection($value));

        }
     }
  }

  function cData($string) {
    global $dom;
    $dom_data = $dom -> createElement('data');
    $ct = $dom_data->ownerDocument->createCDATASection($string);
    return $ct;
  }

  $dbname='cda-spa_tacaje';
  $server = 'mysql:host=localhost;dbname='.$dbname;
  $username = 'root';
  $password = '';
  //$username = 'byytotkb_simone';
  //$password = 'm0l3sk1n3';
  $options = array(
    PDO::ATTR_DEFAULT_FETCH_MODE    => PDO::FETCH_OBJ,
    PDO::ATTR_ERRMODE               => PDO::ERRMODE_EXCEPTION,
    PDO::ATTR_EMULATE_PREPARES      => false
  );
  $db = new PDO($server, $username, $password, $options);
  $db->exec("set names utf8");

  $t = $db->query("SHOW TABLES");
  $tables = $t->fetchAll(PDO::FETCH_ASSOC);
  if (count($tables) > 0) { 
    /* CHECK ESISTENZA TABELLA DI IMPORT */
    $import_table = false;
    foreach($tables as $t) {
      $table_name = $t['Tables_in_'.$dbname];
      if ($table_name == 'import_list') {
        //RIPARTO DA PRECEDENTE SITUAZIONE
        $import_table = true;
      }
    }
    if (!$import_table) {
      //CREO STRUTTURA TABELLA DI IMPORT
      $result = $db->prepare("CREATE TABLE `import_list` (
       `table_name` varchar(100) NOT NULL,
       `offset` int(11) NOT NULL DEFAULT '0',
       `dt` datetime NOT NULL
      ) ENGINE=InnoDB DEFAULT CHARSET=latin1");
      $result->execute();
      //POPOLO TABELLA CON ELENCO TABELLE DA IMPORTARE
      foreach($tables as $t) {
        $table_name = $t['Tables_in_'.$dbname];
        if ($table_name != 'import_list') {
          //RIPARTO DA PRECEDENTE SITUAZIONE
          $result_insert = $db->prepare("INSERT INTO import_list SET table_name = '".$table_name."', offset = 0, dt = NOW()");
          $result_insert->execute();
        }
      }
    }
    /* CHECK ESISTENZA TABELLA DI IMPORT */

    $result_import = $db->prepare('SELECT * FROM import_list');
    $result_import->execute();
    $res_import = $result_import->fetchAll(PDO::FETCH_ASSOC);
    if (count($res_import) > 0) {
    foreach($res_import as $t) {
      $table_name = $t['table_name'];
      $offset = $t['offset'];
      echo '<li>'.$table_name.'</li>';
      switch($table_name) {
        case 'test_prefixtbl_tacajedati_catcataloghi' :
          /*
          //STRUTTURA CATALOGHI (nome file cataloghi_yyyymmddhhmmss)
          id_catalogo: numero univoco catalogo
          catalogo: nome del catalogo
          */
          $limit = 2000;
          $result = $db->prepare('SELECT * FROM '.$table_name.' LIMIT '.$offset.','.$limit);
          $result->execute();
          $json = $result->fetchAll(PDO::FETCH_ASSOC);
          if (count($json) > 0) {
            $data = array();
            foreach($json as $k => $j) {
              $data[] = array(
                'id_catalogo' => $j['idcat'],
                'catalogo' => $j['catalogo']
              );
            }
            writeFile('cataloghi', $data);
            $result_update = $db->prepare("UPDATE import_list SET offset = offset + ".$limit." WHERE table_name = '".$table_name."'");
            $result_update->execute();
            exit;
          } else {
            $result_delete = $db->prepare("DELETE FROM import_list WHERE table_name = '".$table_name."'");
            $result_delete->execute();
          }
        break;
        case 'test_prefixtbl_tacajedati_user' :
          //STRUTTURA UTENZE (nome file utenze_yyyymmddhhmmss)
          // id_user: numero univoco utente
          // numconto: numconto (non univoco)
          // username : email dell'utente
          // password : password dell'utente
          // sconto: maggiorazione sul prezzo indicato nel campo listino e campo promo
          // tipo : tipo utenza (A = agente, C = Cliente, I = Interno)
          // id_escludi_catalogo: escludi la visualizzazione degli id cataloghi indicati
          // promo : nome del listino da associare al campo Promo  (CE, ...)
          // visualizzapromo: 1 = Visualizza colonna Promo 0= non visualizza colonna Promo
          // destinazione: destinazione merce di default
          // listino : nome del listino da associare al campo Listino (LZ, ...)
          // um_clienti: Data ultima modifica
          $limit = 700;
          $result = $db->prepare('SELECT * FROM '.$table_name.' LIMIT '.$offset.','.$limit);
          $result->execute();
          $json = $result->fetchAll(PDO::FETCH_ASSOC);
          if (count($json) > 0) {
            $json_result = array();
            foreach ($json as $r) {
              array_push($json_result, $r);
            }
            if(count($json_result) > 0) {
              writeFile('utenze', $json_result);
            }
            $result_update = $db->prepare("UPDATE import_list SET offset = offset + ".$limit." WHERE table_name = '".$table_name."'");
            $result_update->execute();
            exit;
          } else {
            $result_delete = $db->prepare("DELETE FROM import_list WHERE table_name = '".$table_name."'");
            $result_delete->execute();
          }
        break;
        case 'test_prefixtbl_tacajedati_depositi' :
          //STRUTTURA DEPOSITI (file depositi_yyyymmddhhmmss)
          // id_deposito : codice deposito (09, ...)
          // descr_dep : nome deposito (Pescara, ...)
          $limit = 700;
          $result = $db->prepare('SELECT * FROM '.$table_name.' LIMIT '.$offset.','.$limit);
          $result->execute();
          $json = $result->fetchAll(PDO::FETCH_ASSOC);
          if (count($json) > 0) {
            writeFile('depositi', $json);
            $result_update = $db->prepare("UPDATE import_list SET offset = offset + ".$limit." WHERE table_name = '".$table_name."'");
            $result_update->execute();
            exit;
          } else {
            $result_delete = $db->prepare("DELETE FROM import_list WHERE table_name = '".$table_name."'");
            $result_delete->execute();
          }
        break;
        case 'test_prefixtbl_tacajedati_clienti' :
          //STRUTTURA CLIENTI  (file clienti_yyyymmddhhmmss)
          // agente: id_agente di appartenenza
          // id_deposito : codice deposito (09, ...)
          // ragione_sociale
          // numconto: codice univoco cliente

          // codice_fiscale
          // piva
          // um_clienti: data modifica

          $limit = 2000;
          $result = $db->prepare('SELECT * FROM '.$table_name.' LIMIT '.$offset.','.$limit);
          $result->execute();
          $json = $result->fetchAll(PDO::FETCH_ASSOC);
          if (count($json) > 0) {
            $json_data = array();
            $json_indirizzi = array();
            foreach ($json as $j) {
              $data = array();
              $data['agente'] = $j['agente'];
              $data['numconto'] = $j['Numconto'];
              $data['id_deposito'] = $j['id_deposito'];
              $data['ragione_sociale'] = utf8_encode(htmlspecialchars($j['RagSoc']));
              $data['codice_fiscale'] = $j['CF'];
              $data['piva'] = $j['PIVA'];
              $data['um_clienti'] = $j['UM_Clienti'];
              array_push($json_data,$data);

              $indirizzo = array();
              $indirizzo['numconto'] = $j['Numconto'];
              $indirizzo['via'] = $j['Via'];
              $indirizzo['citta'] = $j['city'];
              $indirizzo['provincia'] = $j['Prov'];
              $indirizzo['cap'] = $j['CAP'];
              $indirizzo['stato'] = $j['Stato'];
              $indirizzo['note'] = $j['Note'];
              array_push($json_indirizzi,$indirizzo);
            }
            if(count($json_data)) {
              writeFile('clienti', $json_data);
            }
            if(count($json_indirizzi)) {
              writeFile('clienti_indirizzi', $json_indirizzi);
            }
            $result_update = $db->prepare("UPDATE import_list SET offset = offset + ".$limit." WHERE table_name = '".$table_name."'");
            $result_update->execute();
            exit;
          } else {
            $result_delete = $db->prepare("DELETE FROM import_list WHERE table_name = '".$table_name."'");
            $result_delete->execute();
          }
        break;
        case 'test_prefixtbl_tacajedati_clienti_indirizzi_aggiuntivi' :
          //STRUTTURA INDIRIZZI CLIENTI (file clienti_indirizzi_yyyymmddhhmmss)
          // numconto: identificativo univoco cliente
          // via
          // citta
          // provincia
          // cap
          // stato
          // note
          $limit = 700;
          $result = $db->prepare('SELECT * FROM '.$table_name.' LIMIT '.$offset.','.$limit);
          $result->execute();
          $json = $result->fetchAll(PDO::FETCH_ASSOC);
          if (count($json) > 0) {
            $json_indirizzi = array();
            foreach ($json as $j) {
              $indirizzo = array();
              $indirizzo['numconto'] = $j['Numconto'];
              $indirizzo['via'] = $j['Via'];
              $indirizzo['citta'] = $j['city'];
              $indirizzo['provincia'] = $j['Prov'];
              $indirizzo['cap'] = $j['CAP'];
              $indirizzo['stato'] = $j['Stato'];
              $indirizzo['note'] = $j['Note'];
              array_push($json_indirizzi,$indirizzo);
            }
            writeFile('clienti_indirizzi', $json_indirizzi);
            $result_update = $db->prepare("UPDATE import_list SET offset = offset + ".$limit." WHERE table_name = '".$table_name."'");
            $result_update->execute();
            exit;
          } else {
            $result_delete = $db->prepare("DELETE FROM import_list WHERE table_name = '".$table_name."'");
            $result_delete->execute();
          }
        break;
        case 'test_prefixtbl_tacajedati_catidgruppi' :
          //STRUTTURA CATEGORIE (file categorie_yyyymmddhhmmss)
          // id: id univoco categoria
          // parent: eventuale parent
          // sottogruppo: nome sottogruppo in italiano
          // sottogruppo_en: nome sottogruppo in inglese
          // sottogruppo_de: nome sottogruppo in tedesco
          // sottogruppo_fr: nome sottogruppo in francese
          // sottogruppo_es: nome sottogruppo in spagnolo
          $limit = 700;
          $result = $db->prepare('SELECT * FROM '.$table_name.' LIMIT '.$offset.','.$limit);
          $result->execute();
          $json = $result->fetchAll(PDO::FETCH_ASSOC);
          if (count($json) > 0) {
            $json_result = array();
            foreach ($json as $r) {
              $obj = array();
              $obj['id'] = $r['idgruppo'];
              if (isset($r['gruppo'])) {
                $obj['sottogruppo'] = $r['gruppo'];
              }
              if (isset($r['gruppo_en'])) {
                $obj['sottogruppo_en'] = $r['gruppo_en'];
              }
              if (isset($r['gruppo_de'])) {
                $obj['sottogruppo_de'] = $r['gruppo_de'];
              }
              if (isset($r['gruppo_fr'])) {
                $obj['sottogruppo_fr'] = $r['gruppo_fr'];
              }
              if (isset($r['gruppo_es'])) {
                $obj['sottogruppo_es'] = $r['gruppo_es'];
              }
              array_push($json_result, $obj);
            }
            writeFile('categorie', $json_result);
            $result_update = $db->prepare("UPDATE import_list SET offset = offset + ".$limit." WHERE table_name = '".$table_name."'");
            $result_update->execute();
            exit;
          } else {
            $result_delete = $db->prepare("DELETE FROM import_list WHERE table_name = '".$table_name."'");
            $result_delete->execute();
          }
        break;
        case 'test_prefixtbl_tacajedati_sottogruppi' :
          //STRUTTURA CATEGORIE (file categorie_yyyymmddhhmmss)
          // id: id univoco categoria
          // parent: eventuale parent
          // sottogruppo: nome sottogruppo in italiano
          // sottogruppo_en: nome sottogruppo in inglese
          // sottogruppo_de: nome sottogruppo in tedesco
          // sottogruppo_fr: nome sottogruppo in francese
          // sottogruppo_es: nome sottogruppo in spagnolo
          $limit = 700;
          $result = $db->prepare('SELECT * FROM '.$table_name.' LIMIT '.$offset.','.$limit);
          $result->execute();
          $json = $result->fetchAll(PDO::FETCH_ASSOC);
          if (count($json) > 0) {
            $json_result = array();
            foreach ($json as $r) {
              $obj = array();
              $obj['id'] = $r['idsottogruppo'];
              $obj['parent'] = $r['gruppo'];
              if (isset($r['sottogruppo'])) {
                $obj['sottogruppo'] = $r['sottogruppo'];
              }
              if (isset($r['sottogruppo_en'])) {
                $obj['sottogruppo_en'] = $r['sottogruppo_en'];
              }
              if (isset($r['sottogruppo_de'])) {
                $obj['sottogruppo_de'] = $r['sottogruppo_de'];
              }
              if (isset($r['sottogruppo_fr'])) {
                $obj['sottogruppo_fr'] = $r['sottogruppo_fr'];
              }
              if (isset($r['sottogruppo_es'])) {
                $obj['sottogruppo_es'] = $r['sottogruppo_es'];
              }
              array_push($json_result, $obj);
            }
            writeFile('categorie', $json_result);
            $result_update = $db->prepare("UPDATE import_list SET offset = offset + ".$limit." WHERE table_name = '".$table_name."'");
            $result_update->execute();
            exit;
          } else {
            $result_delete = $db->prepare("DELETE FROM import_list WHERE table_name = '".$table_name."'");
            $result_delete->execute();
          }
        break;

        case 'test_prefixtbl_tacajedati_riferimenti' :
          //STRUTTURA PRODOTTI  (file prodotti_yyyymmddhhmmss)
          // id: codice univoco prodotto
          // id_catalogo: id della tabella 'catcataloghi'
          // new: 0 non novità, 1 novità
          // OE-AM: OE = Originale   AM=After market
          // descrizione: descrizione del prodotto
          // sottogruppo: id dalla tabella 'tbl_tacaje_sottogruppi'
          // listino: prezzo in Euro consigliato al pubblico
          $limit = 1500;
          $result = $db->prepare('SELECT * FROM '.$table_name.' LIMIT '.$offset.','.$limit);
          $result->execute();
          $json = $result->fetchAll(PDO::FETCH_ASSOC);
          if (count($json) > 0) {
            $json_result = array();
            foreach ($json as &$r) {
              $obj = array();
              $obj['id'] = utf8_encode($r['Riferimento']);
              $obj['id_catalogo'] = $r['id_catalogo'];
              $obj['new'] = $r['New'];
              $obj['oe-am'] = $r['OE-AM'];
              $obj['descrizione'] = $r['descrizione'];
              $obj['sottogruppo'] = $r['id_sottogruppo'];
              $obj['listino'] = $r['Listino'];
              array_push($json_result, $obj);
            }
            if(count($json_result)) {
              writeFile('prodotti', $json_result);
            }
            $result_update = $db->prepare("UPDATE import_list SET offset = offset + ".$limit." WHERE table_name = '".$table_name."'");
            $result_update->execute();
            exit;
          } else {
            $result_delete = $db->prepare("DELETE FROM import_list WHERE table_name = '".$table_name."'");
            $result_delete->execute();
          }
        break;
        case 'test_prefixtbl_tacajedati_listini':
          //STRUTTURA LISTINI  (file listino_yyyymmddhhmmss)
          // id_prodotto: riferimento della tabella 'tbl_tacaje_Riferimenti'
          // listino: identificativo listino
          // netto: prezzo
          // dvala: data update
          $limit = 3000;
          $result = $db->prepare('SELECT * FROM '.$table_name.' LIMIT '.$offset.','.$limit);
          $result->execute();
          $json = $result->fetchAll(PDO::FETCH_ASSOC);
          if (count($json) > 0) {
            $json_result = array();
            foreach ($json as &$r) {
              $obj = array();
              $obj['id_prodotto'] = $r['Riferimento'];
              $obj['listino'] = $r['Listino'];
              $obj['netto'] = $r['Netto'];
              $obj['umd'] = $r['dvala'];
              array_push($json_result, $obj);
            }
            if(count($json_result)) {
              writeFile('listino', $json_result);
            }
            $result_update = $db->prepare("UPDATE import_list SET offset = offset + ".$limit." WHERE table_name = '".$table_name."'");
            $result_update->execute();
            exit;
          } else {
            $result_delete = $db->prepare("DELETE FROM import_list WHERE table_name = '".$table_name."'");
            $result_delete->execute();
          }
        break;
        case 'test_prefixtbl_tacajedati_giacenze':
          //STRUTTURA GIACENZE  (file giacenze_yyyymmddhhmmss)
          // id_prodotto: riferimento della tabella 'tbl_tacaje_Riferimenti'
          // id_deposito: campo della tabella 'tbl_Tacaje_Depositi'
          // stato: campo della tabella 'tbl_Tacaje_stati'
          // umd: data update
          $limit = 4000;
          $result = $db->prepare('SELECT * FROM '.$table_name.' LIMIT '.$offset.','.$limit);
          $result->execute();
          $json = $result->fetchAll(PDO::FETCH_ASSOC);
          if (count($json) > 0) {
            $json_result = array();
            foreach ($json as &$r) {
              $obj = array();
              $obj['id_prodotto'] = $r['Riferimento'];
              $obj['id_deposito'] = $r['id_deposito'];
              $obj['stato'] = $r['stato'];
              $obj['umd'] = $r['umd'];
              array_push($json_result, $obj);
            }

            if(count($json_result)) {
              writeFile('giacenze', $json_result);
            }
            $result_update = $db->prepare("UPDATE import_list SET offset = offset + ".$limit." WHERE table_name = '".$table_name."'");
            $result_update->execute();
            exit;
          } else {
            $result_delete = $db->prepare("DELETE FROM import_list WHERE table_name = '".$table_name."'");
            $result_delete->execute();
          }
        break;
        case 'test_prefixtbl_tacajedati_comparazioni':
          //STRUTTURA CROSS  (file cross_yyyymmddhhmmss)
          // id_prodotto: riferimento della tabella 'tbl_tacaje_Riferimenti'
          // id_catalogo: id della tabella 'catcataloghi'
          // comparazione: codice cross
          // tipo: (Vuoto, ex, sostituente, Componente = suo riquadro, kit = suo riquadro, abbinabile = suo riquadro)
          // note: note
          // data_cross: data update
          $limit = 2500;
          $result = $db->prepare('SELECT * FROM '.$table_name.' LIMIT '.$offset.','.$limit);
          $result->execute();
          $json = $result->fetchAll(PDO::FETCH_ASSOC);
          if (count($json) > 0) {
            $json_result = array();
            foreach ($json as &$r) {
              $obj = array();
              $obj['id_prodotto'] = $r['riferimento'];
              $obj['id_catalogo'] = $r['id_catalogo'];
              $obj['comparazione'] = $r['comparazione'];
              $obj['tipo'] = $r['tipo'];
              $obj['note'] = $r['note'];
              $obj['data_cross'] = $r['data_cross'];
              array_push($json_result, $obj);
            }
            writeFile('cross', $json_result);
            $result_update = $db->prepare("UPDATE import_list SET offset = offset + ".$limit." WHERE table_name = '".$table_name."'");
            $result_update->execute();
            exit;
          } else {
            $result_delete = $db->prepare("DELETE FROM import_list WHERE table_name = '".$table_name."'");
            $result_delete->execute();
          }
        break;
        case 'test_prefixtbl_tacajedati_stati' :
          //STRUTTURA STATI  (file stati_yyyymmddhhmmss)
          // id_stato: identificatore univoco
          // stato: (D, ND, ...)
          // visualizza: 0 non visibile, 1 visibile
          // stato_note: descrizione lunga dello stato
          $limit = 2500;
          $result = $db->prepare('SELECT * FROM '.$table_name.' LIMIT '.$offset.','.$limit);
          $result->execute();
          $json = $result->fetchAll(PDO::FETCH_ASSOC);
          if (count($json) > 0) {
            writeFile('stati', $json);
            $result_update = $db->prepare("UPDATE import_list SET offset = offset + ".$limit." WHERE table_name = '".$table_name."'");
            $result_update->execute();
            exit;
          } else {
            $result_delete = $db->prepare("DELETE FROM import_list WHERE table_name = '".$table_name."'");
            $result_delete->execute();
          }
        break;
        case 'test_prefixtbl_tacajedati_agenti' :
          //STRUTTURA AGENTI (file agenti_yyymmddhhmmss)
          // id : id_Ag
          // deposito : codice deposito
          // agente : nome agente
          // username : email dell'agente
          $limit = 2500;
          $result = $db->prepare('SELECT * FROM '.$table_name.' LIMIT '.$offset.','.$limit);
          $result->execute();
          $json = $result->fetchAll(PDO::FETCH_ASSOC);
          if (count($json) > 0) {
            $data = array();
            foreach($json as $j) {
              $data[] = array(
                'id' => $j['id_Ag'],
                'id_deposito' => $j['Deposito'],
                'agente' => $j['Agente'],
                'username' => $j['mail']
              );
            }
            writeFile('agenti', $data);
            $result_update = $db->prepare("UPDATE import_list SET offset = offset + ".$limit." WHERE table_name = '".$table_name."'");
            $result_update->execute();
            exit;
          } else {
            $result_delete = $db->prepare("DELETE FROM import_list WHERE table_name = '".$table_name."'");
            $result_delete->execute();
          }
        break;
      }
    }
    } else {
      foreach($tables as $t) {
      $table_name = $t['Tables_in_'.$dbname];
      $result_drop = $db->prepare("DROP TABLE ".$table_name);
      $result_drop->execute();
      }
    }
  }
?>
