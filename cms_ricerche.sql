-- phpMyAdmin SQL Dump
-- version 4.2.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Ott 10, 2016 alle 10:35
-- Versione del server: 5.5.34
-- PHP Version: 5.5.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `cda-spa`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `cms_ricerche`
--

CREATE TABLE IF NOT EXISTS `cms_ricerche` (
`id` int(11) NOT NULL,
  `titolo` varchar(100) NOT NULL,
  `ordine` int(5) NOT NULL,
  `params` longtext NOT NULL,
  `active` tinyint(1) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dump dei dati per la tabella `cms_ricerche`
--

INSERT INTO `cms_ricerche` (`id`, `titolo`, `ordine`, `params`, `active`) VALUES
(1, 'Promo', 0, '[{	"field": "categoria.nome",	"value": "PROMO",	"operation": "LIKE"}, {	"field": "cataloghi.catalogo",	"value": "CDA",	"operation": "LIKE"}]', 1),
(4, 'TEST', 1, '[{	"field": "categoria.nome",	"value": "PROMO",	"operation": "LIKE"}, {	"field": "cataloghi.catalogo",	"value": "CDA",	"operation": "LIKE"}]', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cms_ricerche`
--
ALTER TABLE `cms_ricerche`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cms_ricerche`
--
ALTER TABLE `cms_ricerche`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
