-- phpMyAdmin SQL Dump
-- version 4.2.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Set 22, 2016 alle 18:03
-- Versione del server: 5.5.34
-- PHP Version: 5.5.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `cda-spa`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `cataloghi`
--

CREATE TABLE IF NOT EXISTS `cataloghi` (
  `id` int(11) NOT NULL,
  `catalogo` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `categorie`
--

CREATE TABLE IF NOT EXISTS `categorie` (
  `id` int(11) NOT NULL,
  `parent` int(11) DEFAULT NULL,
  `nome` varchar(1000) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `categorie_data`
--

CREATE TABLE IF NOT EXISTS `categorie_data` (
`id` int(11) NOT NULL,
  `id_categoria` int(11) NOT NULL,
  `lingua` varchar(100) NOT NULL,
  `nome` varchar(1000) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=993 ;

-- --------------------------------------------------------

--
-- Struttura della tabella `clienti`
--

CREATE TABLE IF NOT EXISTS `clienti` (
  `id` varchar(255) NOT NULL,
  `id_agente` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `clienti_indirizzi`
--

CREATE TABLE IF NOT EXISTS `clienti_indirizzi` (
`id` int(11) NOT NULL,
  `id_cliente` varchar(255) NOT NULL,
  `ragione_sociale` varchar(300) NOT NULL,
  `via` varchar(300) NOT NULL,
  `citta` varchar(100) NOT NULL,
  `provincia` varchar(50) NOT NULL,
  `cap` varchar(50) NOT NULL,
  `stato` varchar(50) NOT NULL,
  `codice_fiscale` varchar(16) NOT NULL,
  `piva` varchar(50) NOT NULL,
  `note` varchar(300) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struttura della tabella `cms_configurazione`
--

CREATE TABLE IF NOT EXISTS `cms_configurazione` (
`cod` int(11) NOT NULL,
  `site_name` varchar(200) NOT NULL,
  `email` varchar(500) NOT NULL,
  `firma` text NOT NULL,
  `logo` varchar(500) NOT NULL,
  `url` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struttura della tabella `cms_moduli`
--

CREATE TABLE IF NOT EXISTS `cms_moduli` (
  `modulo` varchar(100) NOT NULL,
  `titolo` varchar(100) NOT NULL,
  `ordine` int(5) NOT NULL,
  `origine` varchar(100) DEFAULT NULL,
  `stato` tinyint(1) NOT NULL,
  `icon` varchar(100) DEFAULT NULL,
  `state` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `cms_permessi`
--

CREATE TABLE IF NOT EXISTS `cms_permessi` (
`id` int(11) NOT NULL,
  `codprofilo` int(11) NOT NULL,
  `codmodulo` varchar(100) NOT NULL,
  `view` tinyint(1) NOT NULL,
  `list` tinyint(1) NOT NULL,
  `add` tinyint(1) NOT NULL,
  `edit` tinyint(1) NOT NULL,
  `delete` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struttura della tabella `cms_sessioni`
--

CREATE TABLE IF NOT EXISTS `cms_sessioni` (
`uid` int(11) NOT NULL,
  `user_id` int(11) unsigned NOT NULL,
  `creation_date` int(10) unsigned DEFAULT NULL,
  `token` varchar(50) NOT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=697 ;

-- --------------------------------------------------------

--
-- Struttura della tabella `depositi`
--

CREATE TABLE IF NOT EXISTS `depositi` (
  `id_deposito` varchar(5) NOT NULL,
  `deposito` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `log_errors`
--

CREATE TABLE IF NOT EXISTS `log_errors` (
`id` int(11) NOT NULL,
  `file` varchar(100) NOT NULL,
  `line` int(11) NOT NULL,
  `message` varchar(500) NOT NULL,
  `trace` varchar(1000) NOT NULL,
  `dt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=37 ;

-- --------------------------------------------------------

--
-- Struttura della tabella `prodotti`
--

CREATE TABLE IF NOT EXISTS `prodotti` (
  `id` varchar(255) NOT NULL,
  `id_categoria` int(11) NOT NULL,
  `id_catalogo` int(11) DEFAULT NULL,
  `new` tinyint(1) DEFAULT NULL,
  `oeam` char(2) DEFAULT NULL,
  `text` varchar(1000) DEFAULT NULL,
  `listino` float DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `prodotti_cross`
--

CREATE TABLE IF NOT EXISTS `prodotti_cross` (
`id` int(11) NOT NULL,
  `cross` varchar(255) NOT NULL,
  `id_prodotto` varchar(255) NOT NULL,
  `catalogo` int(11) DEFAULT NULL,
  `tipo` varchar(255) DEFAULT NULL,
  `note` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3955 ;

-- --------------------------------------------------------

--
-- Struttura della tabella `prodotti_giacenze`
--

CREATE TABLE IF NOT EXISTS `prodotti_giacenze` (
`id` int(11) NOT NULL,
  `id_prodotto` varchar(255) NOT NULL,
  `id_deposito` varchar(5) NOT NULL,
  `stato` varchar(10) NOT NULL,
  `dt_update` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10837 ;

-- --------------------------------------------------------

--
-- Struttura della tabella `prodotti_giacenze_stati`
--

CREATE TABLE IF NOT EXISTS `prodotti_giacenze_stati` (
  `id_stato` varchar(20) NOT NULL,
  `stato` varchar(255) NOT NULL,
  `pubblico` tinyint(1) NOT NULL,
  `note` varchar(1000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `prodotti_listini`
--

CREATE TABLE IF NOT EXISTS `prodotti_listini` (
`id` int(11) NOT NULL,
  `id_prodotto` varchar(255) NOT NULL,
  `listino` varchar(5) NOT NULL,
  `prezzo` float NOT NULL,
  `dt` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=24145 ;

-- --------------------------------------------------------

--
-- Struttura della tabella `profili`
--

CREATE TABLE IF NOT EXISTS `profili` (
`id` int(11) NOT NULL,
  `codice` varchar(20) NOT NULL,
  `lock` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=101 ;

-- --------------------------------------------------------

--
-- Struttura della tabella `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`id` int(11) NOT NULL,
  `id_profilo` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `crypt` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1795 ;

-- --------------------------------------------------------

--
-- Struttura della tabella `users_conf`
--

CREATE TABLE IF NOT EXISTS `users_conf` (
  `id_user` int(11) NOT NULL,
  `listino` varchar(255) NOT NULL DEFAULT '',
  `sconto` varchar(20) DEFAULT NULL,
  `promo` char(2) DEFAULT NULL,
  `view_promo` tinyint(1) NOT NULL,
  `destinazione` int(11) DEFAULT NULL,
  `id_cliente` varchar(255) DEFAULT NULL,
  `id_agente` int(11) DEFAULT NULL,
  `agente` varchar(255) DEFAULT NULL,
  `not_cataloghi` varchar(1000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cataloghi`
--
ALTER TABLE `cataloghi`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categorie`
--
ALTER TABLE `categorie`
 ADD PRIMARY KEY (`id`), ADD KEY `parent` (`parent`);

--
-- Indexes for table `categorie_data`
--
ALTER TABLE `categorie_data`
 ADD PRIMARY KEY (`id`), ADD KEY `id_categoria` (`id_categoria`,`lingua`);

--
-- Indexes for table `clienti`
--
ALTER TABLE `clienti`
 ADD PRIMARY KEY (`id`), ADD KEY `id_agente` (`id_agente`);

--
-- Indexes for table `clienti_indirizzi`
--
ALTER TABLE `clienti_indirizzi`
 ADD PRIMARY KEY (`id`), ADD KEY `id_cliente` (`id_cliente`);

--
-- Indexes for table `cms_configurazione`
--
ALTER TABLE `cms_configurazione`
 ADD PRIMARY KEY (`cod`);

--
-- Indexes for table `cms_moduli`
--
ALTER TABLE `cms_moduli`
 ADD PRIMARY KEY (`modulo`);

--
-- Indexes for table `cms_permessi`
--
ALTER TABLE `cms_permessi`
 ADD PRIMARY KEY (`id`), ADD KEY `codprofilo` (`codprofilo`), ADD KEY `codmodulo` (`codmodulo`), ADD KEY `codmodulo_2` (`codmodulo`), ADD KEY `codprofilo_2` (`codprofilo`);

--
-- Indexes for table `cms_sessioni`
--
ALTER TABLE `cms_sessioni`
 ADD PRIMARY KEY (`uid`);

--
-- Indexes for table `depositi`
--
ALTER TABLE `depositi`
 ADD PRIMARY KEY (`id_deposito`);

--
-- Indexes for table `log_errors`
--
ALTER TABLE `log_errors`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `prodotti`
--
ALTER TABLE `prodotti`
 ADD PRIMARY KEY (`id`), ADD KEY `id_categoria` (`id_categoria`), ADD KEY `text` (`text`(255)), ADD KEY `catalogo` (`id_catalogo`);

--
-- Indexes for table `prodotti_cross`
--
ALTER TABLE `prodotti_cross`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `id_prodotto` (`id_prodotto`,`cross`), ADD KEY `catalogo` (`catalogo`,`tipo`);

--
-- Indexes for table `prodotti_giacenze`
--
ALTER TABLE `prodotti_giacenze`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `id_prodotto` (`id_prodotto`,`id_deposito`), ADD KEY `id_deposito` (`id_deposito`);

--
-- Indexes for table `prodotti_giacenze_stati`
--
ALTER TABLE `prodotti_giacenze_stati`
 ADD PRIMARY KEY (`id_stato`);

--
-- Indexes for table `prodotti_listini`
--
ALTER TABLE `prodotti_listini`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `id_prodotto` (`id_prodotto`,`listino`);

--
-- Indexes for table `profili`
--
ALTER TABLE `profili`
 ADD PRIMARY KEY (`id`), ADD KEY `codice` (`codice`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`id`), ADD KEY `id_profilo` (`id_profilo`,`username`,`password`,`crypt`);

--
-- Indexes for table `users_conf`
--
ALTER TABLE `users_conf`
 ADD PRIMARY KEY (`id_user`,`listino`), ADD KEY `id_user` (`id_user`), ADD KEY `id_cliente` (`id_cliente`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categorie_data`
--
ALTER TABLE `categorie_data`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=993;
--
-- AUTO_INCREMENT for table `clienti_indirizzi`
--
ALTER TABLE `clienti_indirizzi`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cms_configurazione`
--
ALTER TABLE `cms_configurazione`
MODIFY `cod` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cms_permessi`
--
ALTER TABLE `cms_permessi`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cms_sessioni`
--
ALTER TABLE `cms_sessioni`
MODIFY `uid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=697;
--
-- AUTO_INCREMENT for table `log_errors`
--
ALTER TABLE `log_errors`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT for table `prodotti_cross`
--
ALTER TABLE `prodotti_cross`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3955;
--
-- AUTO_INCREMENT for table `prodotti_giacenze`
--
ALTER TABLE `prodotti_giacenze`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10837;
--
-- AUTO_INCREMENT for table `prodotti_listini`
--
ALTER TABLE `prodotti_listini`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=24145;
--
-- AUTO_INCREMENT for table `profili`
--
ALTER TABLE `profili`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=101;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1795;
--
-- Limiti per le tabelle scaricate
--

--
-- Limiti per la tabella `categorie`
--
ALTER TABLE `categorie`
ADD CONSTRAINT `categorie_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `categorie` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Limiti per la tabella `categorie_data`
--
ALTER TABLE `categorie_data`
ADD CONSTRAINT `categorie_data_ibfk_1` FOREIGN KEY (`id_categoria`) REFERENCES `categorie` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Limiti per la tabella `clienti_indirizzi`
--
ALTER TABLE `clienti_indirizzi`
ADD CONSTRAINT `clienti_indirizzi_ibfk_1` FOREIGN KEY (`id_cliente`) REFERENCES `clienti` (`id`) ON DELETE CASCADE;

--
-- Limiti per la tabella `cms_permessi`
--
ALTER TABLE `cms_permessi`
ADD CONSTRAINT `cms_permessi_ibfk_1` FOREIGN KEY (`codmodulo`) REFERENCES `cms_moduli` (`modulo`) ON DELETE CASCADE ON UPDATE NO ACTION,
ADD CONSTRAINT `cms_permessi_ibfk_2` FOREIGN KEY (`codprofilo`) REFERENCES `profili` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Limiti per la tabella `prodotti`
--
ALTER TABLE `prodotti`
ADD CONSTRAINT `prodotti_ibfk_1` FOREIGN KEY (`id_categoria`) REFERENCES `categorie` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Limiti per la tabella `prodotti_cross`
--
ALTER TABLE `prodotti_cross`
ADD CONSTRAINT `prodotti_cross_ibfk_1` FOREIGN KEY (`id_prodotto`) REFERENCES `prodotti` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Limiti per la tabella `prodotti_giacenze`
--
ALTER TABLE `prodotti_giacenze`
ADD CONSTRAINT `prodotti_giacenze_ibfk_1` FOREIGN KEY (`id_prodotto`) REFERENCES `prodotti` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
ADD CONSTRAINT `prodotti_giacenze_ibfk_2` FOREIGN KEY (`id_deposito`) REFERENCES `depositi` (`id_deposito`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Limiti per la tabella `prodotti_listini`
--
ALTER TABLE `prodotti_listini`
ADD CONSTRAINT `prodotti_listini_ibfk_1` FOREIGN KEY (`id_prodotto`) REFERENCES `prodotti` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Limiti per la tabella `users`
--
ALTER TABLE `users`
ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`id_profilo`) REFERENCES `profili` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limiti per la tabella `users_conf`
--
ALTER TABLE `users_conf`
ADD CONSTRAINT `users_conf_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
ADD CONSTRAINT `users_conf_ibfk_2` FOREIGN KEY (`id_cliente`) REFERENCES `clienti` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
