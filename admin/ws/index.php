<?php
if(!isset($_SESSION)) session_start();
error_reporting(E_ALL);
ini_set('display_startup_errors', 1);
ini_set('display_errors', 1);
libxml_use_internal_errors(true);
//ini_set("include_path", '/home/rentmyhol/php:' . ini_get("include_path") );
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require 'vendor/autoload.php';

//Common Settings
define ('SITE_URL','http://localhost/cda-spa');
define ('SITE_PATH','/');
define ('API_URL',SITE_URL.'ws/');
define ('UPLOAD_DIR',SITE_PATH.'uploads/');
define ('UPLOAD_URL',SITE_URL.'uploads/');
define('UPLOAD_TEMP',UPLOAD_DIR.'temp/');
define('LIB_PATH',SITE_PATH.'admin/lib/');
define('FILE_PATH','../../file_update/');

// Database information
$settings = array(
    'driver' => 'mysql',
    'host' => 'localhost',
    'database' => 'cda-spa',
    'username' => 'root',
    'password' => '',
    'collation' => 'utf8_general_ci',
    'prefix' => '',
    'charset' => 'utf8',
);
//Common Settings
//Common Functions
function _findexts($filename) {
  $filename = strtolower($filename) ;
  $exts = explode(".", $filename) ;
  $n = count($exts)-1;
  $exts = $exts[$n];
  return $exts;
}
//Common Functions

// Bootstrap Eloquent ORM
$container = new Illuminate\Container\Container;
$connFactory = new \Illuminate\Database\Connectors\ConnectionFactory($container);
$conn = $connFactory->make($settings);
$resolver = new \Illuminate\Database\ConnectionResolver();
$resolver->addConnection('default', $conn);
$resolver->setDefaultConnection('default');
\Illuminate\Database\Eloquent\Model::setConnectionResolver($resolver);

//Slim Framework Init
$configuration = [
    'settings' => [
        'displayErrorDetails' => true,
    ],
];
$c = new \Slim\Container($configuration);
$app = new \Slim\App($c);
$app->get('/', function (Slim\Http\Request $request, Slim\Http\Response $response) {
    $response->write("CDA SPA API");
    $data = $request->getParsedBody();
    return $response;
});
$routeFiles = (array) glob('routes/*.php');
foreach($routeFiles as $routeFile) {
    require $routeFile;
}
$modelsFiles = (array) glob('models/*.php');
foreach($modelsFiles as $modelsFile) {
    require $modelsFile;
}
$app->run();
//Slim Framework Init
