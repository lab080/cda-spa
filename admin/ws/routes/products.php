<?php
$app->group('/products', function () use($app) {
  $this->post('/get', function (Slim\Http\Request $request, Slim\Http\Response $response) {
    $data = $request->getParsedBody();

    /*
    * PARAMETRI DA USARE
    * id_prodotto
    * text : testo del prodotto
    * catalogo
    * categoria
    * limit : numero di risultati nella query
    * offset : numero di elementi da saltare prima di prendere i risultati
    */
    $prodotti = new Prodotti();
    $result = $prodotti->getProdotti($data);

    if(isset($result['message'])) {
      $response = (new Slim\Http\Response())
                  ->withStatus(500, 'KO')
                  ->withHeader('Content-Type', 'application/json')
                  ->write(json_encode($result));
      return $response;
    } else {
      $response = (new Slim\Http\Response())
                  ->withStatus(200, 'OK')
                  ->withHeader('Content-Type', 'application/json')
                  ->write(json_encode($result));
      return $response;
    }
  });

  $this->post('/get-filters', function (Slim\Http\Request $request, Slim\Http\Response $response) {
    $data = $request->getParsedBody();

    /*
    * PARAMETRI DA USARE
    * id_prodotto
    * text : testo del prodotto
    * catalogo
    * categoria
    */
    $prodotti = new Prodotti();
    $result = $prodotti->getFilters($data);

    if(isset($result['message'])) {
      $response = (new Slim\Http\Response())
                  ->withStatus(500, 'OK')
                  ->withHeader('Content-Type', 'application/json')
                  ->write(json_encode($result));
      return $response;
    } else {
      $response = (new Slim\Http\Response())
                  ->withStatus(200, 'OK')
                  ->withHeader('Content-Type', 'application/json')
                  ->write(json_encode($result));
      return $response;
    }
  });

  $this->post('/get-count', function (Slim\Http\Request $request, Slim\Http\Response $response) {
    $data = $request->getParsedBody();

    $prodotti = new Prodotti();
    $result = $prodotti->getCount($data);

    if(isset($result['message'])) {
      $response = (new Slim\Http\Response())
                  ->withStatus(500, 'OK')
                  ->withHeader('Content-Type', 'application/json')
                  ->write(json_encode($result));
      return $response;
    } else {
      $response = (new Slim\Http\Response())
                  ->withStatus(200, 'OK')
                  ->withHeader('Content-Type', 'application/json')
                  ->write(json_encode($result));
      return $response;
    }
  });

  $this->post('/details', function (Slim\Http\Request $request, Slim\Http\Response $response) {
    $data = $request->getParsedBody();

    /*
    * Intero oggetto creato nella get del prodotto richiesto
    */
    //$data = array('id' => '0242229659');
    $prodotti = new Prodotti();
    $result = $prodotti->getDetails($data);

    $response = (new Slim\Http\Response())
                ->withStatus(200, 'OK')
                ->withHeader('Content-Type', 'application/json')
                ->write(json_encode($result));
    return $response;
  });
});
?>
