<?php
$app->group('/import', function () use($app) {
  //==============================================================================
  //=============================== MAIN PROCEDURE ===============================
  //==============================================================================
  $this->get('/sync', function (Slim\Http\Request $request, Slim\Http\Response $response) {
    $ret = false;

    if (!$ret) {
      //=====Import Stati=====
      $cataloghi = new Cataloghi();
      $ret = $cataloghi -> import();
    }

    if (!$ret) {
      //=====Import Stati=====
      $stati = new ProdottiGiacenzeStati();
      $ret = $stati -> import();
    }

    if (!$ret) {
      //=====Import Utenze=====
      $utenze = new User();
      $ret = $utenze -> import();
    }

    if (!$ret) {
      //=====Import Agenti=====
      $agenti = new Agente();
      $ret = $agenti -> import();
    }

    if (!$ret) {
      //=====Import Depositi=====
      $deposito = new Deposito();
      $ret = $deposito -> import();
    }

    if (!$ret) {
      //=====Import Clienti=====
      $cliente = new Cliente();
      $ret = $cliente -> import();
    }

    if (!$ret) {
      //=====Import Indirizzi Clienti=====
      $cliente = new ClienteIndirizzi();
      $ret = $cliente -> import();
    }

    if (!$ret) {
      //=====Import Categorie=====
      $categorie = new Categorie();
      $ret = $categorie -> import();
    }

    if (!$ret) {
      //=====Import Prodotti=====
      $prodotti = new Prodotti();
      $ret = $prodotti -> import();
    }

    if (!$ret) {
      //=====Import Giacenze Prodotti=====
      $prodotti = new ProdottiGiacenze();
      $ret = $prodotti -> import();
    }

    if (!$ret) {
      //=====Import Listini Prodotti=====
      $prodotti = new ProdottiListini();
      $ret = $prodotti -> import();
    }

    if (!$ret) {
      //=====Import Cross Prodotti=====
      $prodotti = new ProdottiCross();
      $ret = $prodotti -> import();
    }

    //Se importato un file refresho per prendere in esame il prossimo
    if($ret) {
      header("Refresh:0");
    }

  });
});
?>
