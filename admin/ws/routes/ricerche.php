<?php
$app->group('/ricerche', function () use($app) {
  $this->post('/save', function (Slim\Http\Request $request, Slim\Http\Response $response) {
    $data = $request->getParsedBody();
    $result = new Ricerche();
    $result -> titolo = $data['name'];
    $result -> ordine = 0;
    $result -> params = json_encode($data['params']);
    $result -> active = 0;
    $result -> save();
    $response = (new Slim\Http\Response())
              ->withStatus(200, 'OK')
              ->withHeader('Content-Type', 'application/json')
              ->write(json_encode('OK'));
    return $response;
  });
  $this->post('/get-active', function (Slim\Http\Request $request, Slim\Http\Response $response) {
    $ricerche = \Ricerche::where('active',1)->orderBy('ordine','asc')->get()->toArray();
    foreach ($ricerche as &$r) {
      $r['params'] = json_decode($r['params']);
    }
    $response = (new Slim\Http\Response())
                ->withStatus(200, 'OK')
                ->withHeader('Content-Type', 'application/json')
                ->write(json_encode($ricerche));
    return $response;
  });
  $this->post('/get-list', function (Slim\Http\Request $request, Slim\Http\Response $response) {
    $ricerche = \Ricerche::orderBy('ordine','asc')->get()->toArray();
    foreach ($ricerche as &$r){
      $r['active'] = (bool)$r['active'];
    }
    $response = (new Slim\Http\Response())
                ->withStatus(200, 'OK')
                ->withHeader('Content-Type', 'application/json')
                ->write(json_encode($ricerche));
    return $response;
  });
  $this->post('/delete', function (Slim\Http\Request $request, Slim\Http\Response $response) {
    $data = $request->getParsedBody();
    if (isset($data['id'])) {
      $result = \Ricerche::find($data['id']);
      $result -> delete();

      $ricerche = \Ricerche::orderBy('ordine','asc')->get()->toArray();
      foreach ($ricerche as &$r){
        $r['active'] = (bool)$r['active'];
      }
      $response = (new Slim\Http\Response())
                  ->withStatus(200, 'OK')
                  ->withHeader('Content-Type', 'application/json')
                  ->write(json_encode($ricerche));
      return $response;
    }
  });
  $this->post('/publish', function (Slim\Http\Request $request, Slim\Http\Response $response) {
    $data = $request->getParsedBody();
    if (isset($data['id'])) {
      $result = \Ricerche::find($data['id']);
      if (!$data['active']) $result -> active = 0;
      else $result -> active = 1;
      $result -> save();
      $response = (new Slim\Http\Response())
                ->withStatus(200, 'OK')
                ->withHeader('Content-Type', 'application/json')
                ->write(json_encode('OK'));
      return $response;
    }
  });
  $this->post('/update-order', function (Slim\Http\Request $request, Slim\Http\Response $response) {
    $data = $request->getParsedBody();
    foreach ($data as $d) {
      $result = \Ricerche::find($d['id']);
      $result -> ordine = $d['ordine'];
      $result -> save();
    }
    $response = (new Slim\Http\Response())
                ->withStatus(200, 'OK')
                ->withHeader('Content-Type', 'application/json')
                ->write(json_encode('OK'));
    return $response;
  });
});
?>
