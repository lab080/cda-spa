<?php
$app->group('/auth', function () use($app) {
  $this->post('/login', function (Slim\Http\Request $request, Slim\Http\Response $response) {
    $data = $request->getParsedBody();
    //Auth Standard
    if (isset($data['u']) && !empty($data['p'])) {
      $result = \User::login($data);
      if($result) {
        unset($result['password']);
        unset($result['crypt']);
        $permissions = \Permessi::getPermissionByProfileId($result['id_profilo']);

        //Utente esiste, creo sessione e restituisco Token
        $session = new Session;
        $token = $session -> generateToken();
        $session -> user_id = $result['id'];
        $session -> creation_date = time();
        $session -> token = $token;
        $session -> save();

        $_SESSION['tokenAdmin'] = $token;
        $_SESSION['user'] = $result;
        //VERIFICO PROFILO
        switch ($result['id_profilo']) {
          //SE 1 -> AGENTE
          //MANDO LISTA CLIENTI
          case 4:
            $clients_list = \Cliente::getClientiByAgente($result['id'])->get()->toArray();
          break;
          //SE 2 -> INTERNO
          //MANDO LISTA CLIENTI COMPLETA
          case 5:
            $clients_list = \Cliente::getAll()->toArray();
          break;
          //SE 3 -> CLIENTE
          //INSERISCO IN SESSIONE il record user_conf dello user
          case 1:
            $user_conf = \UserConfig::getConfUser($result['id'])->get()->toArray();
            $_SESSION['user_conf'] = $user_conf[0];
          break;
        }

        //Generazione messaggio di uscita
        $success = array(
          'token'=>$session -> token, //Mando il token
          'permissions'=>$permissions //Mando i relativi permessi legati all'utente
        );
        if (isset($result)) $success['user'] = $result;
        if (isset($user_conf)) $success['user_conf'] = $user_conf[0];
        if (isset($clients_list)) $success['clients_list'] = $clients_list;
        $response = (new Slim\Http\Response())
                    ->withStatus(200, 'OK')
                    ->withHeader('Content-Type', 'application/json')
                    ->write(json_encode($success));
        return $response;
      } else {
        //Non trovato
        $response = (new Slim\Http\Response())
                    ->withStatus(500)
                    ->withHeader('Content-Type', 'application/json')
                    ->write(json_encode('NOT_FOUND'));
        return $response;
      }
    }
    $response = (new Slim\Http\Response())
                ->withStatus(500)
                ->withHeader('Content-Type', 'application/json')
                ->write(json_encode('NO_DATA'));
    return $response;
  });

  $this->post('/logout', function (Slim\Http\Request $request, Slim\Http\Response $response) {
    unset($_SESSION['tokenAdmin']);
    session_destroy();
    $response = (new Slim\Http\Response())
                ->withStatus(200)
                ->withHeader('Content-Type', 'application/json')
                ->write(true);
    return $response;
  });

  $this->post('/check', function (Slim\Http\Request $request, Slim\Http\Response $response) {
    $token = null;
    $cookieToken = Dflydev\FigCookies\FigRequestCookies::get($request, 'tokenAdmin')->getValue();
    $sessionToken = $request->getParsedBody();
    if ($cookieToken) $token = $cookieToken;
    else if ($sessionToken && isset($sessionToken['token'])) $token = $sessionToken['token'];
    else if ($_SESSION['tokenAdmin'] && isset($_SESSION['tokenAdmin'])) $token = $_SESSION['tokenAdmin'];

    if (preg_match('/^[a-zA-Z0-9]+$/',$token)) {
      $sessione = \Session::whereRaw('token = ?', array($token))->get();
      if (empty($sessione->toArray())) {
          //Log token
          $errore = (object) array('trace'=>'auth.php','message' => 'LOG TOKEN : cookie -> '.$cookieToken.' | session -> '.$sessionToken['token'].' | token -> '.$token);
          \LogException::insertObj($errore);
        //Non trovato
        $response = (new Slim\Http\Response())
                    ->withStatus(500)
                    ->withHeader('Content-Type', 'application/json')
                    ->write(json_encode('TOKEN_NOT_FOUND'));
        return $response;
      } else {
        //Sessione esiste, recupero dati user
        $user = \User::whereRaw('id = ?', array($sessione[0]['user_id']))->get();
        if (!empty($user->toArray())) {
          $response = (new Slim\Http\Response())
                      ->withStatus(200, 'OK')
                      ->withHeader('Content-Type', 'application/json')
                      ->write(json_encode('SUCCESS'));
          return $response;
        } else {
          //Log token
          $errore = (object) array('trace'=>'auth.php','message' => 'LOG TOKEN : cookie -> '.$cookieToken.' | session -> '.$sessionToken['token'].' | token -> '.$token);
          \LogException::insertObj($errore);
          //Non trovato
          $response = (new Slim\Http\Response())
                      ->withStatus(500)
                      ->withHeader('Content-Type', 'application/json')
                      ->write(json_encode('USER_NOT_FOUND'));
          return $response;
        }
      }
    }
    $response = (new Slim\Http\Response())
                ->withStatus(500)
                ->withHeader('Content-Type', 'application/json')
                ->write(json_encode('NO_DATA'));
    return $response;
  });

  $this->post('/forgot-password', function (Slim\Http\Request $request, Slim\Http\Response $response) {
    $data = $request->getParsedBody();
    if (isset($data['u'])) {
      //Verifico esistenza user
      $users = \User::whereRaw('username = ?', array($data['u']))->get();
      if (!empty($users->toArray())) {
        //Invio password
        $u = new User;
        $password = $u->Decrypt($users[0]['password'], $users[0]['crypt']);
        $data = array();
        $data['email'] = $users[0]['username'];
        $u -> sendForgotMail($data,$password);

        $response = (new Slim\Http\Response())
                    ->withStatus(200, 'OK')
                    ->withHeader('Content-Type', 'application/json')
                    ->write(json_encode('EMAIL_SENT'));
        return $response;
      } else {
        //Non trovato
        $response = (new Slim\Http\Response())
                    ->withStatus(500)
                    ->withHeader('Content-Type', 'application/json')
                    ->write(json_encode('USER_NOT_FOUND'));
        return $response;
      }
    }
  });
  //Mandando l'id_user del cliente scelto si cambia la user_conf nella sessione cosi che è come se fosse loggato come il cliente
  $this->post('/choose-client', function (Slim\Http\Request $request, Slim\Http\Response $response) {
    $data = $request->getParsedBody();

    if (isset($data['c'])) {
      //Verifica che sia davvero il suo agente
      $user_conf = \Session::getUserConfigByToken($_SESSION['tokenAdmin']);
      $r_agente = \Agente::where('id_user', $user_conf[0]['id_user'])->get();
      if(count($r_agente)) {
        $id_agente = $r_agente[0]['id'];
      } else {
        $id_agente = false;
      }

      $check_user = \Cliente::checkPermissionCliente($data['c'], $id_agente);

      if($check_user) {
        //Se è tutto a posto allora setto la user_conf
        $user_conf = \UserConfig::getConfUserCliente($data['c'])->get()->toArray();
        if (!empty($user_conf)) {
          $_SESSION['user_conf'] = $user_conf[0];

          $response = (new Slim\Http\Response())
                      ->withStatus(200)
                      ->withHeader('Content-Type', 'application/json')
                      ->write(json_encode($user_conf[0]));
          return $response;
        } else {
          $response = (new Slim\Http\Response())
                    ->withStatus(500)
                    ->withHeader('Content-Type', 'application/json')
                    ->write(json_encode('NO_USER'));
          return $response;
        }
      } else {
        //Non permesso
        $response = (new Slim\Http\Response())
                    ->withStatus(500)
                    ->withHeader('Content-Type', 'application/json')
                    ->write(json_encode('NO_PERMISSION'));
        return $response;
      }
    } else {
      //Non trovato
      $response = (new Slim\Http\Response())
                  ->withStatus(500)
                  ->withHeader('Content-Type', 'application/json')
                  ->write(json_encode('NOT_FOUND'));
      return $response;
    }
  });
});
?>
