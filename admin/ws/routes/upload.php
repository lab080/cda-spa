<?php
$app->post('/upload', function (Slim\Http\Request $request, Slim\Http\Response $response) {
  $file = new \File();
  $destFolder =  UPLOAD_TEMP;
  $ret = array();
  if (!empty($_FILES)) {
    foreach ($_FILES as $f) {
      $error = false;
      //RINOMINA FILE
      $tempFile = $f['tmp_name'];
      $targetPath = UPLOAD_TEMP;
      $ext = $file->findexts($f['name']);
      $nameFile_array = $file->findname($f['name']);
      if (is_array($nameFile_array)) $nameFile = join("_",$nameFile_array);
      else $nameFile = $nameFile_array;
      $nameFile = preg_replace("/ /","_",$nameFile);
      $old_pattern = array("/[^\w ]/", "/_+/", "/_$/");
      $new_pattern = array("_", "_", "");
      $nameFile = strtolower(preg_replace($old_pattern, $new_pattern , $nameFile));
      $fileName = time() . $nameFile . '.' . $ext;
      $targetFile =  str_replace('//','/',$targetPath) ."/". $fileName;
      $fileTypes = array('jpg','jpeg','gif','png','pdf'); // File extensions
      $fileParts = pathinfo($f['name']);

      if (!$error && move_uploaded_file($tempFile,$targetFile)) {
        if ($ext != 'pdf') {
          //RIDIMENSIONAMENTO IMMAGINI
          if ($targetFile && $targetFile != "NULL" && file_exists($targetFile)) {
            $array_sizes = array();
            $array_sizes['thumb']['w'] = 90;
            $array_sizes['thumb']['h'] = 90;
            $array_sizes['thumb']['crop'] = 1;
            $file -> resize ($array_sizes,$fileName,"temp/");
          }
        }
      }
      $ret[] = $fileName;
    }
  }
  $success = array('success'=>'FILE_UPLOADED');
  $response->withStatus(200);
  $response->write(json_encode($ret));
  return $response;
});
?>
