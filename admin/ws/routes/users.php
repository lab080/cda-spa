<?php
$app->group('/users', function () use($app) {
  $this->post('/get-user', function (Slim\Http\Request $request, Slim\Http\Response $response) {
    $data = $request->getParsedBody();
    if (isset($data['id'])) {
      $user = \User::whereRaw('idu = ?', array($data['id']))->get();
      if (!empty($user->toArray())) {
        unset($user[0]['password']);
        $success = $user[0];
        $response = (new Slim\Http\Response())
                    ->withStatus(200, 'OK')
                    ->withHeader('Content-Type', 'application/json')
                    ->write(json_encode($success));
        return $response;
      } else {
        //Non trovato
        $response = (new Slim\Http\Response())
                    ->withStatus(500)
                    ->withHeader('Content-Type', 'application/json')
                    ->write(json_encode('USER_NOT_FOUND'));
        return $response;
      }
    } else {
      $user = \User::all();
      if (!empty($user->toArray())) {
        foreach ($user as $u) {
          unset($u['password']);
        }
        $response = (new Slim\Http\Response())
                    ->withStatus(200, 'OK')
                    ->withHeader('Content-Type', 'application/json')
                    ->write(json_encode($user));
        return $response;
      } else {
        //Non trovato
        $response = (new Slim\Http\Response())
                    ->withStatus(500)
                    ->withHeader('Content-Type', 'application/json')
                    ->write(json_encode('USER_NOT_FOUND'));
        return $response;
      }
    }
  });
  $this->post('/save', function (Slim\Http\Request $request, Slim\Http\Response $response) {
    $data = $request->getParsedBody();

    if (isset($data['idu'])) {
      $users = \User::whereRaw('idu = ?', array($data['idu']))->get();
      if (!empty($users->toArray())) {
        $updateData = array();
        $updateData['utente'] = $data['utente'];
        $updateData['username'] = $data['username'];

        $user = new \User();
        $encodedPwd = $user -> encodedPwd($data['password']);
        $updateData['password'] = $encodedPwd;

        $affectedRows = \User::whereRaw('idu = ?', array($data['idu']))->update($updateData);
        $item = $users->toArray();

        //Utente aggiornato
        $success = array('success'=>'USER_CONFIRMED');
        $response->withStatus(200);
        $response->write(json_encode($success));
        return $response;
      } else {
        //Utente non trovato
        $error = array('error'=>'USER_NOT_FOUND');
        $response->withStatus(500);
        $response->write(json_encode($error));
        return $response;
      }
    } else {
      //Nuovo inserimento
      $user = new \User();
      $user->utente = $data['utente'];
      $user->username = $data['username'];
      $user->livello = 1;
      $user->password = $user->encodedPwd($data['password']);
      $user->save();
      $userId = $user->id;

      //Utente aggiornato
      $success = array('success'=>'USER_CONFIRMED');
      $response->withStatus(200);
      $response->write(json_encode($success));
      return $response;
    }
  });
  $this->post('/delete', function (Slim\Http\Request $request, Slim\Http\Response $response) {
    $data = $request->getParsedBody();
    if ($data['idu']) {
      $users = \User::whereRaw('idu = ?', array($data['idu']))->delete();
    }

    $user = \User::all();
    if (!empty($user->toArray())) {
      foreach ($user as $u) {
        unset($u['password']);
      }
      $response = (new Slim\Http\Response())
                  ->withStatus(200, 'OK')
                  ->withHeader('Content-Type', 'application/json')
                  ->write(json_encode($user));
      return $response;
    } else {
      //Non trovato
      $response = (new Slim\Http\Response())
                  ->withStatus(500)
                  ->withHeader('Content-Type', 'application/json')
                  ->write(json_encode('USER_NOT_FOUND'));
      return $response;
    }
  });

  /*$this->get('/generate_user', function (Slim\Http\Request $request, Slim\Http\Response $response) {
    $username = 'admin@admin.it';
    $password = '123456789';

    $user = new User();
    $user->generateUser($username, $password);
  });*/
});
?>
