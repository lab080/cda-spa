<?php
$app->group('/conf', function () use($app) {
  $this->get('/get-conf', function (Slim\Http\Request $request, Slim\Http\Response $response) {
    $data = $request->getParsedBody();
  });
  $this->get('/languages', function (Slim\Http\Request $request, Slim\Http\Response $response) {
    $lingue = \Lingue::all();
    $response = (new Slim\Http\Response())
                ->withStatus(200, 'OK')
                ->withHeader('Content-Type', 'application/json')
                ->write(json_encode($lingue));
    return $response;
  });
  $this->post('/modules', function (Slim\Http\Request $request, Slim\Http\Response $response) {
    $data = $request->getParsedBody();
    $modules = array();
    $module = array('id'=>'user','state'=>'logged.users.list','location'=>'','label'=>'Utenti');
    array_push($modules,$module);

    $response = (new Slim\Http\Response())
                ->withStatus(200, 'OK')
                ->withHeader('Content-Type', 'application/json')
                ->write(json_encode($modules));
    return $response;
  });

  $this->post('/permissions', function (Slim\Http\Request $request, Slim\Http\Response $response) {
    $data = $request->getParsedBody();
    $permissions = array();

    $module = array('id'=>'user');
    $p = array('add'=>true,'edit'=>true,'delete'=>true,'view'=>true,'list'=>true);
    $module['p'] = $p;
    array_push($permissions,$module);

    $response = (new Slim\Http\Response())
                ->withStatus(200, 'OK')
                ->withHeader('Content-Type', 'application/json')
                ->write(json_encode($permissions));
    return $response;
  });
});
?>
