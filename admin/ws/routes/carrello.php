<?php
$app->group('/carrello', function () use($app) {
  $this->post('/get', function (Slim\Http\Request $request, Slim\Http\Response $response) {
    $data = $request->getParsedBody();

    /*
    * PARAMETRI DA USARE
    * id_user
    * id_utente
    */

    /*$data = array(
      'id_user' => 7642,
      'id_cliente' => 1011
    );*/

    if($data['id_user']) {
      $carrello = new Carrello();
      $result = $carrello->getByUser($data)->get();

      $response = (new Slim\Http\Response())
                  ->withStatus(200, 'OK')
                  ->withHeader('Content-Type', 'application/json')
                  ->write(json_encode($result));
      return $response;
    } else {
      $response = (new Slim\Http\Response())
                  ->withStatus(500, 'KO')
                  ->withHeader('Content-Type', 'application/json')
                  ->write(json_encode(array('message' => 'Non hai inserito nessun id utente')));
      return $response;
    }
  });

  $this->post('/save', function (Slim\Http\Request $request, Slim\Http\Response $response) {
    $data = $request->getParsedBody();

    /*
    * PARAMETRI DA USARE
      'id' => se presente esegue una update
      'id_user'
      'id_cliente'
      'id_prodotto'
      'quantita'
      'prezzo'
      'deposito'
    */

    /*$data = array(
      'id' => 4,
      'id_user' => 7642,
      'id_cliente' => 1013,
      'id_prodotto'=> "10",
      'quantita'=> 10,
      'prezzo'=> 10,
      'deposito'=> "10"
    );*/

    if($data['id_user'] && $data['id_prodotto'] && $data['quantita'] && $data['prezzo'] && $data['deposito']) {
      $result = \Carrello::saveRow($data);

      $response = (new Slim\Http\Response())
                  ->withStatus(200, 'OK')
                  ->withHeader('Content-Type', 'application/json')
                  ->write(json_encode($result));
      return $response;
    } else {
      $response = (new Slim\Http\Response())
                  ->withStatus(500, 'KO')
                  ->withHeader('Content-Type', 'application/json')
                  ->write(json_encode(array('message' => 'Alcuni parametri obbligatori non sono presenti')));
      return $response;
    }

  });

  $this->post('/delete', function (Slim\Http\Request $request, Slim\Http\Response $response) {
    $data = $request->getParsedBody();

    /*
    * PARAMETRI DA USARE
    * id
    */

    /*$data = array(
      'id' => 4
    );*/

    if($data['id']) {
      $result = \Carrello::destroy($data['id']);

      $response = (new Slim\Http\Response())
                  ->withStatus(200, 'OK')
                  ->withHeader('Content-Type', 'application/json')
                  ->write(json_encode($result));
      return $response;
    } else {
      $response = (new Slim\Http\Response())
                  ->withStatus(500, 'KO')
                  ->withHeader('Content-Type', 'application/json')
                  ->write(json_encode(array('message' => 'Non hai inserito nessun id')));
      return $response;
    }
  });
});
?>
