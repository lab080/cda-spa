<?php
use Illuminate\Database\Eloquent\Builder;
class ProdottiGiacenze extends \Illuminate\Database\Eloquent\Model{
	protected $table = 'prodotti_giacenze';
	protected $primaryKey = 'id';
	public function import() {
		$start = microtime(true);
		echo "import giacenze prodotti<br />";
		$Import = new Import();
		$filename = $Import->findFile('giacenze');
		if($filename) {
			echo $filename;
    		//Leggo il file per ottenere il contenuto
			$result = $Import->readFile($filename);
			foreach($result as $key => &$r) {
                //===================
                //PROCEDURA GIACENZE
                //===================
				if (isset($r['id_prodotto']) && isset($r['id_deposito'])) {
					$p = \Prodotti::find($r['id_prodotto']);
					if ($p) {
						try {
							$f = \ProdottiGiacenze::  where('id_prodotto', '=', $r['id_prodotto'])
							->where('id_deposito', '=', $r['id_deposito'])
							->first();

							if (!$f) {
								$f = new \ProdottiGiacenze();
							}
						} catch(Exception $e) {
							\LogException::insert($e);
							//return false;
						}

						$f -> id_prodotto = $r['id_prodotto'];
						$f -> id_deposito = $r['id_deposito'];
						$f -> stato = $r['stato'];
						$f -> dt_update = $r['umd'];
						$f -> updated_at = null;

						try {
							$f -> save();
						} catch(Exception $e) {
							print_r($e->getMessage());
							\LogException::insert($e);
							//return false;
						}
					} else {
						$errore = (object) array('trace'=>$filename,'message' => 'Prodotto '.$r['id_prodotto'].' non esistente');
						\LogException::insertObj($errore);
					}
				}

//===================
//PROCEDURA GIACENZE
//===================

//============================ FILE SAVE =======================================
        //Cancello l'oggetto appena inserito
				unset($result[$key]);

                //Controllo l'esecuzione del file
				$time_elapsed_secs = microtime(true) - $start;

                //Salvo dove sono arrivato e refresho
				if($time_elapsed_secs > 20) {
					$Import->writeFile($filename, $result);
					header("Refresh:0");
				}
			}
	        $Import->finishFile($filename);
			return true;
		} else {
			echo "nessun file trovato<br />";
			echo "<br />";
			return false;
		}
	}
}
?>
