<?php
use Illuminate\Database\Eloquent\Builder;
class Carrello extends \Illuminate\Database\Eloquent\Model{
    protected $table = 'cms_carrello';

    public static function scopeGetByUser($query, $data) {
      $query->where('id_user', $data['id_user']);
      if(isset($data['id_cliente'])) { $query->where('id_cliente', $data['id_cliente']); }
      return $query;
    }

    public static function saveRow($data) {
      if(isset($data['id'])) {
        $carrello = \Carrello::find($data['id']);
      } else {
        $carrello = new Carrello();
      }

      $carrello->id_user = $data['id_user'];
      if(isset($data['id_cliente'])) $carrello->id_cliente = $data['id_cliente'];
      $carrello->id_prodotto = $data['id_prodotto'];
      $carrello->quantita = $data['quantita'];
      $carrello->prezzo = $data['prezzo'];
      $carrello->deposito = $data['deposito'];
      $carrello->updated_at = null;

      $carrello->save();
    }
}
