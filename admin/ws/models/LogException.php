<?php
use Illuminate\Database\Eloquent\Builder;
class LogException extends \Illuminate\Database\Eloquent\Model{
    protected $table = 'log_errors';
    public $timestamps = false;

    //Inserimento nuovo profilo con restituzione dell'id numerico
    public static function insert($e) {
      $data = new LogException;
      $data->file = $e->getFile();
      $data->line = $e->getLine();
      $data->message = $e->getMessage();
      $data->trace = $e->getTraceAsString();
      $data->save();
    }

    public static function insertObj($e) {
      $data = new LogException;
      $data->message = $e->message;
      if (isset($e->trace)) $data->trace = $e->trace;
      $data->save();
    }
}
