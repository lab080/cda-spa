<?php
class Import {
  private $path = '../../file_update/';
  private $working = '../../file_update/working/';
  private $completed = '../../file_update/completed/';

  //Legge un file e restituisce l'oggetto gia decodificato dal json
  private function getFile($file, $path) {
    if (file_exists($path.$file)) {
      $fh = fopen($path.$file, 'r');
      $json = fread($fh, filesize($path.$file));
      fclose($fh);
      $xml = simplexml_load_string($json,'SimpleXMLElement', LIBXML_NOCDATA);
      $json = json_encode($xml, JSON_PRETTY_PRINT);
      $items = (array) json_decode($json, TRUE);
      return $items['item'];
    } else return false;
  }

  private function getFileJson($file, $path) {
    if (file_exists($path.$file)) {
      $fh = fopen($path.$file, 'r');
      $json = fread($fh, filesize($path.$file));
      fclose($fh);
      return (array) json_decode($json, TRUE);
    } else return false;
  }

  //Cerca un nome di un file all'interno della cartella principale (es: utenze, depositi, ecc..)
  public function findFile($name) {
    $files = scandir($this->path);
    foreach($files as $f) {
      if($f !== '.' && $f !== '..') {
        $split = explode('_', $f);
        unset($split[ count($split)-1 ]);
        $string = join('_', $split);

        if($string === $name) {
          return $f;
        }
      }
    }
    return false;
  }

  //Controlla se il file da leggere è gia in working.. se non c'e lo cerca nella cartella principale
  public function readFile($file) {

    $result = $this->getFileJson($file, $this->working);
    if($result) { return $result; }

    $result = $this->getFile($file, $this->path);
    if($result) { return $result; }

    return false;

  }

  //Scrive il file nella cartella working
  public function writeFile($file, $result) {
    try {
      $fh = fopen($this->working.$file, 'w');
      fwrite($fh, json_encode($result));
      fclose($fh);
      return true;
    } catch(Exception $e) {
      \LogException::insert($e);
      return false;
    }
  }

  //Funzione da avviare quando è finita la lettura del file
  public function finishFile($file) {
    try {
      //Elimino il file in working
      if (file_exists($this->working.$file)) unlink($this->working.$file);
      //Sposto il file del path principale nel folder completed
      rename($this->path.$file, $this->completed.$file);
      return true;
    } catch(Exception $e) {
      \LogException::insert($e);
      return false;
    }
  }

}
?>
