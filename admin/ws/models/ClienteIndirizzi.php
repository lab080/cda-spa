<?php
use Illuminate\Database\Eloquent\Builder;
class ClienteIndirizzi extends \Illuminate\Database\Eloquent\Model{
    protected $table = 'clienti_indirizzi';
    public $timestamps = false;


    public static function updateOrInsert($values) {
      if(!isset($values['id_cliente'])) { return false; }
      $result = \ClienteIndirizzi::where('id_cliente', $values['id_cliente'])->get();

      $data = new ClienteIndirizzi;
      if(count($result)) {
        $data = \ClienteIndirizzi::find($result[0]['id']);
      }

      $data->id_cliente = $values['id_cliente'];
      if(isset($values['via'])) { $data->via = $values['via']; }
      if(isset($values['citta'])) { $data->citta = $values['citta']; }
      if(isset($values['provincia'])) { $data->provincia = $values['provincia']; }
      if(isset($values['cap'])) { $data->cap = $values['cap']; }
      if(isset($values['stato'])) { $data->stato = $values['stato']; }
      if(isset($values['note']) && gettype($values['note']) !== 'array') { $data->note = $values['note']; }
      if(isset($values['primary'])) { $data->primary = $values['primary']; }

      $data->save();
      return $data->id;
    }

    public function import() {
      $start = microtime(true);
      echo "import clienti indirizzi<br />";
      $Import = new Import();
      $filename = $Import->findFile('clienti_indirizzi');
      if($filename) {
        //Leggo il file per ottenere il contenuto
            $result = $Import->readFile($filename);
            //numconto    via    citta    provincia    cap    stato   note
            foreach($result as $key => &$r) {

//========================= CLIENTI INDIRIZZI ==================================
                $r['id_cliente'] = $r['numconto'];
                try {
                  \ClienteIndirizzi::updateOrInsert($r);
                } catch(Exception $e) {
                  \LogException::insert($e);
                }

//============================ FILE SAVE =======================================
                //Cancello l'oggetto appena inserito
                unset($result[$key]);

                //Controllo l'esecuzione del file
                $time_elapsed_secs = microtime(true) - $start;

                //Salvo dove sono arrivato e refresho
                if($time_elapsed_secs > 20) {
                  $Import->writeFile($filename, $result);
                  header("Refresh:0");
                }
            }
          $Import->finishFile($filename);
          return true;
      } else {
          echo "nessun file trovato<br />";
          echo "<br />";
          return false;
      }
    }

}
