<?php
use Illuminate\Database\Eloquent\Builder;
class Prodotti extends \Illuminate\Database\Eloquent\Model{
    protected $table = 'prodotti';

    public function getFilters($data) {
      if(!isset($_SESSION['user_conf'])) {
        return array('message' => 'SESSION user_conf not exist');
      }
      $start = microtime(true);

      $query = Prodotti::search($data)->getId();
      $sql = $query->toSql();

      foreach($query->getBindings() as $binding) {
        $value = is_numeric($binding) ? $binding : "'".$binding."'";
        $sql = preg_replace('/\?/', $value, $sql, 1);
      }

      $res['categorie'] = \Prodotti::getCategorie($sql)->get();
      $res['marche'] = \Prodotti::getMarche($sql)->get();
      $res['cataloghi'] = \Prodotti::getCataloghi($sql)->get();

      //GET
      $rows = $query->getCampiRicerca()->filtersData($data)->get();
      $res['data'] = count($rows);

      $time_elapsed_secs = microtime(true) - $start;
      return array('values' => $res, 'time' => $time_elapsed_secs);
    }

    public function getCount($data) {
      if(!isset($_SESSION['user_conf'])) {
        return array('message' => 'SESSION user_conf not exist');
      }
      $start = microtime(true);

      $count = Prodotti::search($data)
                      ->filtersData($data)
                      ->getCampiRicerca()
                      ->get();

      $time_elapsed_secs = microtime(true) - $start;
      return array('count' => count($count), 'time' => $time_elapsed_secs);
    }

    public function getProdotti($data) {
      if(!isset($_SESSION['user_conf'])) {
        return array('message' => 'SESSION user_conf not exist');
      }
      $start = microtime(true);

      $res = Prodotti::search($data)
                      ->filtersData($data)
                      ->pagination($data)
                      ->getCampiRicerca()
                      ->get();

      //Calcolo netto
      foreach($res as &$r) {
        //Se > 100 devo mostrare solo il prezzo del prodotto
        //Se < 100 devo mostare prezzo_netto con lo sconto applicato
        if($_SESSION['user_conf']['sconto'] > 100) {
          $r['prezzo_listino'] = number_format($r['prezzo_listino'], 2, '.', '');
          unset($r['prezzo_netto']);
        } else {
          $prezzo = $r['prezzo_netto'] - (($r['prezzo_netto']/100)*$_SESSION['user_conf']['sconto']);
          $r['prezzo_netto'] = number_format($prezzo, 2, '.', '');
        }
        $r['prezzo_listino'] = number_format($r['prezzo_listino'], 2, '.', '');
        $r['immagine'] = 'http://www.cda-web.net/tacaje/files/picture/'.$r['id_prodotto'].'.jpg';
      }

      $time_elapsed_secs = microtime(true) - $start;
      return array('values' => $res, 'time' => $time_elapsed_secs, 'sconto' => $_SESSION['user_conf']['sconto']);
    }

    public function scopeGetCategorie($query, $query_sql) {
      return $query->selectRaw('categorie.id, categorie.nome')
                  ->join('categorie', 'categorie.id', '=', 'prodotti.id_categoria')
                  ->whereRaw('prodotti.id IN ('.$query_sql.')')
                  ->orderBy('categorie.nome')
                  ->groupBy('categorie.id');
    }

    public function scopeGetMarche($query, $query_sql) {
      return $query->selectRaw('cataloghi.id, cataloghi.catalogo as nome')
                  ->join('cataloghi', 'cataloghi.id', '=', 'prodotti.id_catalogo')
                  ->whereRaw('prodotti.id IN ('.$query_sql.')')
                  ->orderBy('cataloghi.catalogo')
                  ->groupBy('cataloghi.id');
    }

    public function scopeGetCataloghi($query, $query_sql) {
      return $query->selectRaw('cataloghi.id, cataloghi.catalogo as nome')
                  ->from('prodotti_cross')
                  ->join('cataloghi', 'cataloghi.id', '=', 'prodotti_cross.id_catalogo')
                  ->whereRaw('prodotti_cross.id_prodotto IN ('.$query_sql.')')
                  ->orderBy('cataloghi.catalogo')
                  ->groupBy('cataloghi.id');
    }

    public static function scopeGetId($query) {
      return $query->select('prodotti.id');
    }

    public static function scopeGetCampiRicerca($query) {
      $select = array(
        'prodotti.new',
        'prodotti.oeam',
        'prodotti.text',
        'prodotti.listino as prezzo_listino',
        'prodotti.id as id_prodotto',
        'prodotti_listini.listino',
        'prodotti_listini.prezzo as prezzo_netto',
        'prodotti_giacenze.id_deposito',
        'prodotti_giacenze_stati.stato as id_stato',
        'prodotti_giacenze_stati.note as stato',
        'cataloghi.catalogo',
        'categorie_data.nome as categoria'
      );
      return $query->select($select);
    }

    public static function scopePagination($query, $data) {
      if(isset($data['limit'])) { $query->limit($data['limit']); }
      if(isset($data['offset'])) { $query->offset($data['offset']); }
      return $query;
    }

    public static function scopeFiltersData($query, $data) {
      //Applicazione filtri
      if(isset($data['filtersData'])) {
        $fields = array();
        $import = array();
        foreach($data['filtersData'] as $filter) {
          if($filter['field'] == 'prodotti_cross.id_catalogo') $import['prodotti_cross'] = true;

          if(!isset($fields[$filter['field']])) { $fields[$filter['field']] = array(); }
          if(isset($filter['value'])) {
            if(isset($filter['value']['id'])) {
              $fields[$filter['field']][] = $filter['value']['id'];
            } else {
              $fields[$filter['field']][] = $filter['value'];
            }
          } else {
            $fields[$filter['field']][] = 1;
          }
        }

        if(isset($import['prodotti_cross'])) {
          $query->join('prodotti_cross', 'prodotti.id_catalogo', '=', 'prodotti_cross.id_catalogo');
          $query->groupBy('prodotti.id');
        }

        foreach($fields as $field => $value) {
          switch($field) {
            case 'prodotti.id' :
            case 'prodotti.listino' :
              $query->join('prodotti_cross', 'prodotti.id_catalogo', '=', 'prodotti_cross.id_catalogo');
              $query->groupBy('prodotti.id');
              //$query->where($field, 'LIKE', '%'.$value[0].'%');
              $query->whereRaw('(prodotti.id LIKE "%'.$value[0].'%" OR prodotti_cross.cross LIKE "%'.$value[0].'%")');
            break;
            default :
              $where = array();
              foreach($value as $v) $where[] = $field.' = '.$v;
              $query->whereRaw('('.join(' OR ',$where).')');
            break;
          }
        }
      }
      return $query;
    }

    public static function scopeSearch($query) {

      $users_conf = $_SESSION['user_conf'];
      $users_conf['lingua'] = 'it';

      //JOIN CON LISTINI
      $query->join('prodotti_listini', 'prodotti_listini.id_prodotto', '=', 'prodotti.id');
      $query->join('prodotti_giacenze', 'prodotti_giacenze.id_prodotto', '=', 'prodotti.id');
      $query->join('prodotti_giacenze_stati', 'prodotti_giacenze.stato', '=', 'prodotti_giacenze_stati.stato');
      $query->join('cataloghi', 'cataloghi.id', '=', 'prodotti.id_catalogo');
      $query->join('categorie_data', 'categorie_data.id_categoria', '=', 'prodotti.id_categoria');

      //CONFIGURO LA QUERY IN BASE ALL'UTENTE
      if($users_conf['view_promo']) {
        $listino = $users_conf['promo'];
      } else {
        $listino = $users_conf['listino'];
      }

      //SESSION WHERE
      $query->where('prodotti_listini.listino', $listino);
      $query->whereIn('prodotti_giacenze.id_deposito', explode(',', $users_conf['id_depositi']));
      $query->where('categorie_data.lingua',  $users_conf['lingua']);

      //RETURN QUERY STRING WITH BINDINGS
      $query->select('prodotti.id');
      return $query;
    }

    public static function getDetails($data) {
      $start = microtime(true);

      $users_conf = $_SESSION['user_conf'];
      $users_conf['lingua'] = 'it';

      $res = Prodotti::where('prodotti.id', '=', $data['id'])
                      ->search()
                      ->getCampiRicerca()
                      ->get();

      if($res) {
        $crossData = array();
        $cross = new ProdottiCross();
        foreach($res as &$r) {
          $elements = $cross::getCross($r['id_prodotto']);

          //Elimino tutti i risultati di tipo ex
          foreach($elements  as $key => $e) {
            if($e['tipo'] == 'ex') {
              unset($elements[$key]);
            }
          }

          $crossData = $elements;
        }
      }

      $time_elapsed_secs = microtime(true) - $start;
      return array('values' => $res[0], 'cross' => $crossData, 'time' => $time_elapsed_secs);
    }

    public function import() {
      $start = microtime(true);
    	echo "import prodotti<br />";
    	$Import = new Import();
    	$filename = $Import->findFile('prodotti');
    	if($filename) {
            echo $filename;
    		//Leggo il file per ottenere il contenuto
            $result = $Import->readFile($filename);
            foreach($result as $key => &$r) {

                //===================
                //PROCEDURA PRODOTTI
                //===================
                if (isset($r['id'])) {
                  $f = \Prodotti::find($r['id']);
                  if (!$f) {
                    $f = new \Prodotti();
                    $f -> id = $r['id'];
                  }
                } else {
                  $f = new \Prodotti();
                }
                $f -> id_categoria = $r['sottogruppo'];
                //$f -> cda_tn = $r['cda_tn'];
                $f -> id_catalogo = $r['id_catalogo'];
                $f -> new = $r['new'];
                $f -> oeam = $r['oe-am'];
                $f -> text = $r['descrizione'];
                $f -> listino = $r['listino'];
                $f -> updated_at = null;
                try {
                  $f -> save();
                } catch(Exception $e) {
                  \LogException::insert($e);
                }


                //===================
                //PROCEDURA PRODOTTI
                //===================

                //============================ FILE SAVE =======================================
                //Cancello l'oggetto appena inserito
                unset($result[$key]);

                //Controllo l'esecuzione del file
                $time_elapsed_secs = microtime(true) - $start;

                //Salvo dove sono arrivato e refresho
                if($time_elapsed_secs > 20) {
                  $Import->writeFile($filename, $result);
                  header("Refresh:0");
                }
            }
	        $Import->finishFile($filename);
            return true;
    	} else {
        	echo "nessun file trovato<br />";
        	echo "<br />";
        	return false;
        }
    }
}
?>
