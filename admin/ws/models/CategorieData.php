<?php
use Illuminate\Database\Eloquent\Builder;
class CategorieData extends \Illuminate\Database\Eloquent\Model{
  protected $table = 'categorie_data';
  public $timestamps = false;
  public $primaryKey = 'cod';
  public $foreignKey = 'id_categoria';
  public function scopeByOrigin($query,$id)
  {
    return $query->where('id_categoria', $id);
  }
}
