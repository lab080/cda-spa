<?php
use Illuminate\Database\Eloquent\Builder;
class Cliente extends \Illuminate\Database\Eloquent\Model{
    protected $table = 'clienti';
    public $incrementing = false;
    public $timestamps = false;

    public static function checkPermissionCliente($id_cliente, $id_agente) {
      $res = \Cliente::where('id', $id_cliente)->where('id_agente', $id_agente)->get();
      if($res) {
        return true;
      } else {
        return false;
      }
    }

    public static function scopeGetClientiByAgente($query,$id) {
      $id_agente = \Agente::getId($id);
      return $query ->where('id_agente', $id_agente)
                      ->join('users_conf', 'users_conf.id_cliente', '=', 'clienti.id')
                      ->orderBy('clienti.ragione_sociale','asc')
                      ->groupBy('clienti.id');
    }

    public static function scopeGetAll($query) {
      return $query ->join('users_conf', 'users_conf.id_cliente', '=', 'clienti.id')
                    ->orderBy('clienti.ragione_sociale','asc')
                    ->groupBy('clienti.id')->get();
    }

    public static function updateOrInsert($values) {
      if(!isset($values['numconto'])) { return false; }
      $data = \Cliente::find($values['numconto']);

      if(!$data) {
        $data = new Cliente;
      }

      $data->id = $values['numconto'];
      $data->id_agente = $values['agente'];
      $data->ragione_sociale = $values['ragione_sociale'];
      if(isset($values['codice_fiscale']) && gettype($values['codice_fiscale']) !== 'array') { $data->codice_fiscale = $values['codice_fiscale']; }
      if(isset($values['codice_fiscale']) && gettype($values['codice_fiscale']) !== 'array') { $data->piva = $values['piva']; }

      $depositi = array_filter(explode(',' , $data->id_depositi));
      $depositi[] = $values['id_deposito'];
      $depositi = array_unique($depositi);
      $data->id_depositi = join(',' , $depositi);

      $data->save();
      return $data->id;
    }

    public static function addDeposito($id_user = false, $deposito = false) {
      if(!$id_user || $deposito) { return false; }

      $db = \Cliente::find($data['id_user']);

      if($db) {

        $depositi = explode(',' , $db->id_deposito);
        $depositi[] = $deposito;
        $depositi = array_unique($depositi);
        $db->id_deposito = join(',' , $depositi);
        $db->save();

        return true;
      } else {
        return false;
      }
    }

    public function import() {
      $start = microtime(true);
      echo "import clienti<br />";
      $Import = new Import();
      $filename = $Import->findFile('clienti');
      if($filename) {
        //Leggo il file per ottenere il contenuto
            $result = $Import->readFile($filename);
            //agente   numconto   id_deposito   ragione_sociale   codice_fiscale   piva
            foreach($result as $key => &$r) {

//============================== CLIENTI =======================================
                $r['id_cliente'] = \Cliente::updateOrInsert($r);


//============================ FILE SAVE =======================================
                //Cancello l'oggetto appena inserito
                unset($result[$key]);

                //Controllo l'esecuzione del file
                $time_elapsed_secs = microtime(true) - $start;

                //Salvo dove sono arrivato e refresho
                if($time_elapsed_secs > 20) {
                  $Import->writeFile($filename, $result);
                  header("Refresh:0");
                }
            }
          $Import->finishFile($filename);
          return true;
      } else {
          echo "nessun file trovato<br />";
          echo "<br />";
          return false;
      }
    }

}
