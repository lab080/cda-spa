<?php
use Illuminate\Database\Eloquent\Builder;
class Profiles extends \Illuminate\Database\Eloquent\Model{
    protected $table = 'profili';
    public $timestamps = false;

    //Controlla se è presente il codice profilo nel db, altrimenti restituisce false
    public static function getId($code) {
      $result = \Profiles::where('codice', $code)->get();
      foreach($result as $r) {
        return $r['id'];
      }
      return false;
    }

    //Inserimento nuovo profilo con restituzione dell'id numerico
    public static function insertCode($code) {
      $id_profilo = \Profiles::getId($code);
      if(!$id_profilo) {

        $data = new Profiles;
        $data->codice = $code;
        $data->save();
        return $data->id;

      } else {
        return $id_profilo;
      }
    }

}
