<?php
use Illuminate\Database\Eloquent\Builder;
class UserConfig extends \Illuminate\Database\Eloquent\Model{
    protected $table = 'users_conf';
    protected $primaryKey = 'id_user';
    public $timestamps = false;
    
    public function scopeGetConfUser($query,$id)
    {
        return $query ->selectRaw(' users_conf.*, 
                                    IF(users_conf.id_depositi IS NOT NULL, users_conf.id_depositi, (SELECT id_depositi FROM clienti WHERE users_conf.id_cliente = clienti.id)) as id_depositi,
                                    (SELECT ragione_sociale FROM clienti WHERE users_conf.id_cliente = clienti.id) as ragione_sociale')
                      ->where('id_user', $id);
    }
    public function scopeGetConfUserCliente($query,$id)
    {
        return $query ->selectRaw('users_conf.*, 
                                  (SELECT id_depositi FROM clienti WHERE users_conf.id_cliente = clienti.id) as id_depositi,
                                  (SELECT ragione_sociale FROM clienti WHERE users_conf.id_cliente = clienti.id) as ragione_sociale')
                      ->where('id_cliente', $id)
                      ->limit(1);
    }
    public static function updateOrInsert($data) {

      $db = \UserConfig::find($data['id_user']);

      if(!$db) {
        $db = new UserConfig;
      }

      $db->id_user = $data['id_user'];
      $db->listino = $data['listino'];
      $db->sconto = $data['sconto'];
      $db->promo = $data['promo'];
      $db->view_promo = $data['visualizzapromo'];
      $db->destinazione = $data['destinazione'];
      if(gettype($data['id_deposito']) !== 'array') $db->id_depositi = $data['id_deposito'];
      if(isset($data['id_escludi_catalogo']) && gettype($data['id_escludi_catalogo']) !== 'array') $db->not_cataloghi = $data['id_escludi_catalogo'];
      if(isset($data['numconto']) && gettype($data['numconto']) !== 'array') $db->id_cliente = $data['numconto'];

      $db->save();

    }

    public static function addDeposito($id_user = false, $deposito = false) {
      if(!$id_user || $deposito) { return false; }

      $db = \UserConfig::find($data['id_user']);

      if($db) {

        $depositi = explode(',' , $db->id_deposito);
        $depositi[] = $deposito;
        $depositi = array_unique($depositi);
        $db->id_deposito = join(',' , $depositi);
        $db->save();

        return true;
      } else {
        return false;
      }
    }
}
