<?php
use Illuminate\Database\Eloquent\Builder;
class Agente extends \Illuminate\Database\Eloquent\Model{
    protected $table = 'agenti';
    public $timestamps = false;

    public static function updateOrInsert($values) {
      if(!isset($values['id_user']) && !isset($values['agente'])) { return false; }
      $result = \Agente::where('id_user', $values['id_user'])->get();

      $data = new Agente;
      if(count($result)) {
        $data = \Agente::find($result[0]['id']);
      }

      $data->id = $values['id'];
      $data->id_user = $values['id_user'];
      $data->agente = $values['agente'];

      $data->save();
      return $data->id;
    }

    public function import() {
      $start = microtime(true);
      echo "import agenti<br />";
      $Import = new Import();
      $filename = $Import->findFile('agenti');
      if($filename) {
        //Leggo il file per ottenere il contenuto
            $result = $Import->readFile($filename);
            //id   deposito   agente   username
            foreach($result as $key => &$r) {

//============================== AGENTI ========================================
                //Ricerca id agente
                if(isset($r['username'])) {
                  $username = \User::where('username', $r['username'])->get();
                  try {
                    $r['id_user'] = $username[0]['id'];
                  } catch(Exception $e) {
                    $r['id_user'] = null;
                  }
                  //Se lo trovo aggiungo il possibile deposito su cui puo operare
                  \UserConfig::addDeposito($r['id_user'], $r['id_deposito']);
                }

                if($r['id_user'] > 0) {
                  \Agente::updateOrInsert($r);
                }

//============================ FILE SAVE =======================================
                //Cancello l'oggetto appena inserito
                unset($result[$key]);

                //Controllo l'esecuzione del file
                $time_elapsed_secs = microtime(true) - $start;

                //Salvo dove sono arrivato e refresho
                if($time_elapsed_secs > 20) {
                  $Import->writeFile($filename, $result);
                  header("Refresh:0");
                }
            }
          $Import->finishFile($filename);
          return true;
      } else {
          echo "nessun file trovato<br />";
          echo "<br />";
          return false;
      }
    }

}
