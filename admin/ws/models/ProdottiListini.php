<?php
use Illuminate\Database\Eloquent\Builder;
class ProdottiListini extends \Illuminate\Database\Eloquent\Model{
	protected $table = 'prodotti_listini';
	protected $primaryKey = 'id';
	public function import() {
		$start = microtime(true);
		echo "import listino prodotti<br />";
		$Import = new Import();
		$filename = $Import->findFile('listino');
		if($filename) {
			echo $filename;
    		//Leggo il file per ottenere il contenuto
			$result = $Import->readFile($filename);
			foreach($result as $key => &$r) {
                //===================
                //PROCEDURA GIACENZE
                //===================
				if(gettype($r['id_prodotto']) === 'array') { unset($r['id_prodotto']); }
				if (isset($r['id_prodotto']) && isset($r['listino'])) {
					$p = \Prodotti::find($r['id_prodotto']);
					if ($p) {
						try {
							$f = \ProdottiListini::  where('id_prodotto', '=', $r['id_prodotto'])
							->where('listino', '=', $r['listino'])
							->first();
							if (!$f) {
								$f = new \ProdottiListini();
							}
						} catch(Exception $e) {
							print_r($e->getMessage());
							\LogException::insert($e);
							//return false;
						}

						$f -> id_prodotto = $r['id_prodotto'];
						$f -> listino = $r['listino'];
						$f -> prezzo = $r['netto'];
						$f -> dt = $r['dvala'];
						$f -> updated_at = null;
						try {
							$f -> save();
						} catch(Exception $e) {
							print_r($e->getMessage());
							\LogException::insert($e);
							//return false;
						}
					} else {
						$errore = (object) array('trace'=>$filename,'message' => 'Prodotto '.$r['id_prodotto'].' non esistente');
						\LogException::insertObj($errore);
					}
				}

                //===================
                //PROCEDURA GIACENZE
                //===================

                //============================ FILE SAVE =======================================
                //Cancello l'oggetto appena inserito
				unset($result[$key]);

                //Controllo l'esecuzione del file
				$time_elapsed_secs = microtime(true) - $start;

                //Salvo dove sono arrivato e refresho
				if($time_elapsed_secs > 28) {
					$Import->writeFile($filename, $result);
					header("Refresh:0");
				}
			}
	        $Import->finishFile($filename);
			return true;
		} else {
			echo "nessun file trovato<br />";
			echo "<br />";
			return false;
		}
	}
}
?>
