<?php
use Illuminate\Database\Eloquent\Builder;
class Deposito extends \Illuminate\Database\Eloquent\Model{
    protected $table = 'depositi';
    protected $primaryKey = 'id_deposito';
    public $incrementing = false;
    public $timestamps = false;

    public static function updateOrInsert($data) {

      $db = \Deposito::find($data['id_deposito']);

      if(!$db) {
        $db = new Deposito;
      }

      $db->id_deposito = $data['id_deposito'];
      $db->deposito = $data['deposito'];

      $db->save();
      return $db->id_deposito;

    }

    public function import() {
      $start = microtime(true);
      echo "import depositi<br />";
      $Import = new Import();
      $filename = $Import->findFile('depositi');
      if($filename) {
        echo $filename;
        //Leggo il file per ottenere il contenuto
            $result = $Import->readFile($filename);
            //'id_deposito' 'deposito'
            foreach($result as $key => &$r) {

//============================= DEPOSITI =======================================
                if (isset($r['id_deposito'])) {
                  $f = \Deposito::find($r['id_deposito']);
                  if (!$f) {
                    $f = new \Deposito();
                    $f -> id_deposito = $r['id_deposito'];
                  }
                } else {
                  $f = new \Deposito();
                }
                $f -> deposito = $r['descr_dep'];
                $f -> save();

//============================ FILE SAVE =======================================
                //Cancello l'oggetto appena inserito
                unset($result[$key]);

                //Controllo l'esecuzione del file
                $time_elapsed_secs = microtime(true) - $start;

                //Salvo dove sono arrivato e refresho
                if($time_elapsed_secs > 20) {
                  $Import->writeFile($filename, $result);
                  header("Refresh:0");
                }
            }
          $Import->finishFile($filename);
          return true;
      } else {
          echo "nessun file trovato<br />";
          echo "<br />";
          return false;
      }
    }
}
