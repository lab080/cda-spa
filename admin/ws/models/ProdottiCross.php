<?php
use Illuminate\Database\Eloquent\Builder;
class ProdottiCross extends \Illuminate\Database\Eloquent\Model{
	protected $table = 'prodotti_cross';
	protected $primaryKey = 'id';

	public static function scopeGetCross($query, $id) {
		return $query->join('prodotti', 'prodotti_cross.cross', '=', 'prodotti.id')->where('cross', $id)->get();
	}

	public function import() {
		$start = microtime(true);
		echo "import cross prodotti<br />";
		$Import = new Import();
		$filename = $Import->findFile('cross');
		if($filename) {
			echo $filename;
    		//Leggo il file per ottenere il contenuto
			$result = $Import->readFile($filename);
			foreach($result as $key => &$r) {
                //===================
                //PROCEDURA GIACENZE
                //===================
				if(gettype($r['id_prodotto']) === 'array') { unset($r['id_prodotto']); }
				if (isset($r['id_prodotto']) && isset($r['comparazione'])) {
					$p = \Prodotti::find($r['id_prodotto']);
					if ($p) {
						try {
							$f = \ProdottiCross::  where('id_prodotto', '=', $r['id_prodotto'])
							->where('cross', '=', $r['comparazione'])
							->first();
							if (!$f) {
								$f = new \ProdottiCross();
							}
						} catch(Exception $e) {
							\LogException::insert($e);
							//return false;
						}

						$f -> id_prodotto = $r['id_prodotto'];
						$f -> id_catalogo = $r['id_catalogo'];
						$f -> cross = $r['comparazione'];
						if (!is_array($r['tipo'])) $f -> tipo = $r['tipo'];
						if (!is_array($r['note'])) $f -> note = $r['note'];
						$f -> updated_at = null;
						try {
							$f -> save();
						} catch(Exception $e) {
							print_r($e->getMessage());
							\LogException::insert($e);
							//return false;
						}
					} else {
						$errore = (object) array('trace'=>$filename,'message' => 'Prodotto '.$r['id_prodotto'].' non esistente');
						\LogException::insertObj($errore);
					}
				}

                //===================
                //PROCEDURA GIACENZE
                //===================

                //============================ FILE SAVE =======================================
                //Cancello l'oggetto appena inserito
				unset($result[$key]);

                //Controllo l'esecuzione del file
				$time_elapsed_secs = microtime(true) - $start;

                //Salvo dove sono arrivato e refresho
				if($time_elapsed_secs > 20) {
					$Import->writeFile($filename, $result);
					header("Refresh:0");
				}
			}
	        $Import->finishFile($filename);
			return true;
		} else {
			echo "nessun file trovato<br />";
			echo "<br />";
			return false;
		}
	}
}
?>
