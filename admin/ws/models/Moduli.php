<?php
class Moduli extends \Illuminate\Database\Eloquent\Model{
    protected $table = 'cms_moduli';
    protected $primaryKey = 'modulo';

    public $incrementing = false;
    public $timestamps = false;
}
