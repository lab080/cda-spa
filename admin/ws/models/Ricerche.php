<?php
class Ricerche extends \Illuminate\Database\Eloquent\Model{
    protected $table = 'cms_ricerche';
    protected $primaryKey = 'id';
    public $timestamps = false;
}
