<?php
use Illuminate\Database\Eloquent\Builder;
class ProdottiGiacenzeStati extends \Illuminate\Database\Eloquent\Model{
    protected $table = 'prodotti_giacenze_stati';
    protected $primaryKey = 'id_stato';
    public $timestamps = false;
    
    public function import() {
      $start = microtime(true);
    	echo "import prodotti giacenze stati<br />";
    	$Import = new Import();
    	$filename = $Import->findFile('stati');
    	if($filename) {
            echo $filename;
    		//Leggo il file per ottenere il contenuto
            $result = $Import->readFile($filename);
            foreach($result as $key => &$r) {

                //===================
                //PROCEDURA STATI
                //===================
                if (isset($r['idstato'])) {
                  $f = \ProdottiGiacenzeStati::find($r['idstato']);
                  if (!$f) {
                    $f = new \ProdottiGiacenzeStati();
                    $f -> id_stato = $r['idstato'];
                  }
                } else {
                  $f = new \ProdottiGiacenzeStati();
                }
                $f -> stato = $r['stato'];
                $f -> pubblico = $r['visualizza'];
                $f -> note = $r['stato_note'];
                $f -> save();

                //===================
                //PROCEDURA STATI
                //===================

                //============================ FILE SAVE =======================================
                //Cancello l'oggetto appena inserito
                unset($result[$key]);

                //Controllo l'esecuzione del file
                $time_elapsed_secs = microtime(true) - $start;

                //Salvo dove sono arrivato e refresho
                if($time_elapsed_secs > 20) {
                  $Import->writeFile($filename, $result);
                  header("Refresh:0");
                }
            }
	        $Import->finishFile($filename);
            return true;
    	} else {
        	echo "nessun file trovato<br />";
        	echo "<br />";
        	return false;
        }
    }
}
?>
