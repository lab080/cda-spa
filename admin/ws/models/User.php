<?php
use Illuminate\Database\Eloquent\Builder;
class User extends \Illuminate\Database\Eloquent\Model{
    protected $table = 'users';

    /*public static function generateUser($username, $password) {
      $db = new User;
      $crypt = $db->generateCode();
      $password = $db->Crypt($password, $crypt);

      echo 'USERNAME -> '.$username.'<br>';
      echo 'PASSWORD -> '.$password.'<br>';
      echo 'CRYPT -> '.$crypt.'<br>';
    }*/

    public static function login($data) {
      if(!isset($data['u']) && !isset($data['p'])) { return false; }
      $db = new User;
      $values = \User::where('username', $data['u'])->get()->toArray();
      foreach($values as $v) {
        $password = $db->Decrypt($v['password'], $v['crypt']);
        if($password === $data['p']) {
          return $v;
        }
      }
      return false;
    }

    public static function updateOrInsert($data) {
      $result = \User::where('username', $data['username'])->get();

      $db = new User;
      if(count($result)) {
        $db = \User::find($result[0]['id']);
      }

      //crypt
      $data['crypt'] = $db->generateCode();
      $data['password'] = $db->Crypt($data['password'], $data['crypt']);

      $db->id_profilo = $data['id_profilo'];
      $db->username = $data['username'];
      $db->password = $data['password'];
      $db->crypt = $data['crypt'];
      $db->updated_at = null;

      $db->save();
      return $db->id;

    }

    public function generateCode($length=15)
    {
      $stringa = "QWERTYUIOPASDFGHJKLZXCVBNMqwertyuiopasdfghjklzxcvbnm01234567890"; //Caratteri usabili nella password
      $fine = strlen ($stringa);

      srand(); //inizializzazione processo di generazione dei numeri casuali
      $out = "";
      for ($i=0; $i<$length;$i++) {
        $roll = intval(rand(0,$fine));
        $out .= substr ($stringa, $roll, 1);
      }
      return $out;
    }

    public function sendConfirmationMail($data) {
        if (isset($data['email']) && $data['email']) {
            $body = "Link di conferma: ".API_URL.'registration/confirm-registration/'.$data['codice'];
            mail($data['email'],"Conferma la tua iscrizione",$body);
        }
    }

    public function sendRegistrationMail($data) {
        if (isset($data['email']) && $data['email']) {
            $body = "Registrazione confermata";
            mail($data['email'],"Registrazione confermata",$body);
        }
    }

    public function sendForgotMail($data,$pwd) {
        if (isset($data['email']) && $data['email'] && $pwd) {
            $mail = new Mail();
            $options['from'] = 'storrisi@gmail.com';
            $options['fromName'] = 'Tacaje';
            $options['subject'] = '[Tacaje] Password dimenticata';
            $options['receiver'] = $data['email'];
            $options['bcc'] = 'storrisi@gmail.com';
            $options['body'] = "Nuova password: ".$pwd;
            $mail -> sendMail($options);
        }
    }

    public function encodedPwd($pwd) {
        return sha1(md5($pwd));
    }

//============================== CODIFICA  =====================================

    public function Crypt($string, $key) {
      $result = '';
      for($i=0; $i<strlen($string); $i++) {
      $char = substr($string, $i, 1);
      $keychar = substr($key, ($i % strlen($key))-1, 1);
      $char = chr(ord($char)+ord($keychar));
      $result.=$char;
      }

      return base64_encode($result);
    }

    public function Decrypt($string, $key) {
      $result = '';
      $string = base64_decode($string);

      for($i=0; $i<strlen($string); $i++) {
      $char = substr($string, $i, 1);
      $keychar = substr($key, ($i % strlen($key))-1, 1);
      $char = chr(ord($char)-ord($keychar));
      $result.=$char;
      }

      return $result;
    }

//============================= / CODIFICA  ====================================

    public function import() {
      $start = microtime(true);
      echo "import utenti<br />";
      $Import = new Import();
      $filename = $Import->findFile('utenze');
      if($filename) {
        //Leggo il file per ottenere il contenuto
            $result = $Import->readFile($filename);
            //numconto username password sconto tipo id_escludi_catalogo id_deposito promo visualizza promo destinazione listino um_clienti
            foreach($result as $key => &$r) {

//============================= PROFILI ========================================
                //Inserimento del nuovo profilo se non esiste ancora
                $r['id_profilo'] = \Profiles::insertCode($r['tipo']);

//============================== USERS =========================================
                //Inserimento utenza se non esiste ancora
                $r['id_user'] = \User::updateOrInsert($r);

//============================= USER_CONF ======================================
                //Inserimento informazioni utenza
                \UserConfig::updateOrInsert($r);

//============================ FILE SAVE =======================================
                //Cancello l'oggetto appena inserito
                unset($result[$key]);

                //Controllo l'esecuzione del file
                $time_elapsed_secs = microtime(true) - $start;

                //Salvo dove sono arrivato e refresho
                if($time_elapsed_secs > 20) {
                  $Import->writeFile($filename, $result);
                  header("Refresh:0");
                }
            }
          $Import->finishFile($filename);
          return true;
      } else {
          echo "nessun file trovato<br />";
          echo "<br />";
          return false;
      }
    }
}
