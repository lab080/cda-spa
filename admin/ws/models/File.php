<?php
class File {
  public function resize($array_sizes,$file,$is_temp = null) {
		foreach ($array_sizes as $size => $s) {
			require_once realpath(LIB_PATH.'phpthumb/ThumbLib.inc.php');
			$sizew = $s['w'];
			$sizeh = $s['h'];
			$dir_path = UPLOAD_DIR."/cache/".$sizew."_".$sizeh;
			$file_path = $dir_path."/".$file;
			if (!file_exists($dir_path)) mkdir($dir_path,0755);
			if (!file_exists($file_path)) {
				$thumb = PhpThumbFactory::create(UPLOAD_DIR."/".$is_temp.$file);
				if ($s['crop']) {
					if (isset($s['orientation'])) {
						switch ($s['orientation']) {
							case 'L':
								$thumb->resize($sizew);
							break;
							case 'P':
								$thumb->resize(0,$sizeh);
							break;
						}
					}
					$thumb->adaptiveResize($sizew,$sizeh);
				} elseif (isset($s['orientation'])) {
					switch ($s['orientation']) {
						case 'L':
							$thumb->resize($sizew);
						break;
						case 'P':
							$thumb->resize(0,$sizeh);
						break;
					}
				} else $thumb->resize($sizew,$sizeh);
				$thumb->save($file_path);
				chmod($file_path, 0755);
			}
		}
	}

  public function deleteFile($filename)
	{
		foreach(new recursiveIteratorIterator( new recursiveDirectoryIterator(UPLOAD_DIR)) as $file)
		{
			/*** if the file is found ***/
			$file_found = explode("/",$file);
			if( $filename == end($file_found))
			{
				unlink($file);
			}
		}
	}

  public function findexts($filename) {
		$filename = strtolower($filename) ;
		$exts = explode(".", $filename) ;
		$n = count($exts)-1;
		$exts = $exts[$n];
		return $exts;
	}

  public function findname($filename) {
		$filename = strtolower($filename) ;
		$exts = explode(".", $filename) ;
		$array = array_pop($exts);
		return $exts;
	}
}
?>
