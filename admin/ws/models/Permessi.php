<?php
class Permessi extends \Illuminate\Database\Eloquent\Model{
    protected $table = 'cms_permessi';

    public $timestamps = false;

    public static function getPermissionByProfileId($profile = 0)
    {
      $view = array(
        'cms_moduli.modulo',
        'cms_moduli.titolo',
        'cms_moduli.icon',
        'cms_moduli.state',
        'cms_moduli.origine',
        'cms_permessi.view',
        'cms_permessi.list',
        'cms_permessi.add',
        'cms_permessi.edit',
        'cms_permessi.delete'
      );

      $result = \Permessi::where('codprofilo', $profile)
                          ->join('cms_moduli', 'cms_moduli.modulo', '=', 'cms_permessi.codmodulo')
                          ->orderBy('cms_moduli.ordine', 'asc')
                          ->where('cms_moduli.stato', 1)
                          ->get($view);

      $permessi = new Permessi;
      $data = array();
      foreach($result as &$r) {
        if($r['origine'] == null) {
          $children = $permessi->buildTree($result, $r['modulo']);
          if(!empty($children)){
            $r['children'] = $children;
          }
          $data[] = $r;
        }
      }

      return $data;
    }

    private function buildTree($elements, $parentId = 0) {
        $branch = array();

        foreach ($elements as $element) {
            if ($element['origine'] == $parentId) {
                $permessi = new Permessi;
                $children = $permessi->buildTree($elements, $element['modulo']);
                if ($children) {
                    $element['children'] = $children;
                }
                $branch[] = $element;
            }
        }

        return $branch;
    }
}
