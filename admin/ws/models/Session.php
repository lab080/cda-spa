<?php
class Session extends \Illuminate\Database\Eloquent\Model{
    protected $table = 'cms_sessioni';
    public $timestamps = false;

    public function generateToken($length=50)
    {
      $stringa = "QWERTYUIOPASDFGHJKLZXCVBNMqwertyuiopasdfghjklzxcvbnm01234567890"; //Caratteri usabili nella password
      $fine = strlen ($stringa);

      srand(); //inizializzazione processo di generazione dei numeri casuali
      $out = "";
      for ($i=0; $i<$length;$i++) {
        $roll = intval(rand(0,$fine));
        $out .= substr ($stringa, $roll, 1);
      }
      return $out;
    }

    public static function getUserConfigByToken($token) {
      return \Session::where('token', $token)
                      ->join('users_conf', 'users_conf.id_user', '=', 'cms_sessioni.user_id')
                      ->get();
    }
}
