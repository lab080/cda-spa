<?php
use Illuminate\Database\Eloquent\Builder;
class Categorie extends \Illuminate\Database\Eloquent\Model{
    protected $table = 'categorie';
    protected $primaryKey = 'id';

    public function import() {
    	$start = microtime(true);
    	echo "import categorie<br />";
    	$Import = new Import();
    	$filename = $Import->findFile('categorie');
    	if($filename) {
            echo $filename;
    		$result = $Import->readFile($filename);
            foreach($result as $key => &$r) {
                //===================
                //PROCEDURA CATEGORIE
                //===================

                //CATEGORIE
                if (isset($r['id']) && is_numeric($r['id'])) {
                  $f = \Categorie::find($r['id']);
                  if (!$f) {
                    $f = new \Categorie();
                  }
                } else {
                  $f = new \Categorie();
                }
                $f -> id = $r['id'];
                if (isset($r['parent'])) $f -> parent = $r['parent'];
                if (isset($r['sottogruppo'])) $f -> nome = $r['sottogruppo'];
                $f -> updated_at = null;
                $f -> save();
                //TRADUZIONI
                $c = \CategorieData::byOrigin($r['id'])->delete();
                if (isset($r['sottogruppo'])) { 
                    $c = new \CategorieData();
                    $c -> id_categoria = $r['id'];
                    $c -> lingua = 'it';
                    $c -> nome = $r['sottogruppo'];
                    $c -> save();
                }
                if (isset($r['sottogruppo_en'])) {
                    $c = new \CategorieData();
                    $c -> id_categoria = $r['id'];
                    $c -> lingua = 'en';
                    $c -> nome = $r['sottogruppo_en'];
                    $c -> save();
                }
                if (isset($r['sottogruppo_de'])) {
                    $c = new \CategorieData();
                    $c -> id_categoria = $r['id'];
                    $c -> lingua = 'de';
                    $c -> nome = $r['sottogruppo_de'];
                    $c -> save();
                }
                if (isset($r['sottogruppo_fr'])) {
                    $c = new \CategorieData();
                    $c -> id_categoria = $r['id'];
                    $c -> lingua = 'fr';
                    $c -> nome = $r['sottogruppo_fr'];
                    $c -> save();
                }
                if (isset($r['sottogruppo_es'])) {
                    $c = new \CategorieData();
                    $c -> id_categoria = $r['id'];
                    $c -> lingua = 'es';
                    $c -> nome = $r['sottogruppo_es'];
                    $c -> save();
                }

                //===================
                //PROCEDURA CATEGORIE
                //===================

                //============================ FILE SAVE =======================================
                //Cancello l'oggetto appena inserito
                unset($result[$key]);

                //Controllo l'esecuzione del file
                $time_elapsed_secs = microtime(true) - $start;

                //Salvo dove sono arrivato e refresho
                if($time_elapsed_secs > 20) {
                  $Import->writeFile($filename, $result);
                  header("Refresh:0");
                }
            }
	        $Import->finishFile($filename);
            return true;
    	} else {
        	echo "nessun file trovato<br />";
        	echo "<br />";
        	return false;
        }
    }
}
?>