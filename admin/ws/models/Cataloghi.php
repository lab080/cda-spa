<?php
use Illuminate\Database\Eloquent\Builder;
class Cataloghi extends \Illuminate\Database\Eloquent\Model{
    protected $table = 'cataloghi';
    protected $primaryKey = 'id';
    public $timestamps = false;
    
    public function import() {
      $start = microtime(true);
    	echo "import cataloghi<br />";
    	$Import = new Import();
    	$filename = $Import->findFile('cataloghi');
    	if($filename) {
            echo $filename;
    		//Leggo il file per ottenere il contenuto
            $result = $Import->readFile($filename);
            foreach($result as $key => &$r) {

                //===================
                //PROCEDURA CATALOGHI
                //===================
                if (isset($r['id_catalogo'])) {
                  $f = \Cataloghi::find($r['id_catalogo']);
                  if (!$f) {
                    $f = new \Cataloghi();
                    $f -> id = $r['id_catalogo'];
                  }
                } else {
                  $f = new \Cataloghi();
                }
                $f -> catalogo = $r['catalogo'];
                $f -> save();

                //===================
                //PROCEDURA CATALOGHI
                //===================

                //============================ FILE SAVE =======================================
                //Cancello l'oggetto appena inserito
                unset($result[$key]);

                //Controllo l'esecuzione del file
                $time_elapsed_secs = microtime(true) - $start;

                //Salvo dove sono arrivato e refresho
                if($time_elapsed_secs > 20) {
                  $Import->writeFile($filename, $result);
                  header("Refresh:0");
                }
            }
	        $Import->finishFile($filename);
            return true;
    	} else {
        	echo "nessun file trovato<br />";
        	echo "<br />";
        	return false;
        }
    }
}
?>
