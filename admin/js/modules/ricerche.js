'use strict';
/* Controllers */
angular.module('cmsApp.ricerche', [])
.controller('RicercheCtrl', function($scope,$rootScope,Ricerche,$state,moduleObj) {
    console.log(moduleObj);
	if (!_.isEmpty(moduleObj)) {
		$scope.permissions = moduleObj;
		$scope.doInsert = function(data) {
			if ($scope.permissions.add) {
				Ricerche.set(new Object());
				$state.go('logged.ricerche.edit');
			}
		}
		$scope.doEdit = function(data) {
			if ($scope.permissions.edit) {
				console.log(data);
				Ricerche.set(data);
				$state.go('logged.ricerche.edit');
			}
		};
		$scope.doSave = function(data) {
			if ($scope.permissions.edit) Ricerche.save(data).then(function(data){$state.go('logged.ricerche.list');},function(error){});
		};
		$scope.doDelete = function(data) {
			if (confirm('Confermi la cancellazione?')) {
				if ($scope.permissions.delete)  {
					Ricerche.del(data).then(function(data){
						$rootScope.$broadcast('Ricerche',data);
					},function(error){});
				}
			}
		}
		$scope.doActive = function(data,status) {
			if ($scope.permissions.edit) Ricerche.publish(data,status);
		};
	}
})
.controller('RicercheListCtrl', function($scope,$rootScope,Ricerche) {
	console.log("RicercheListCtrl");

	if ($scope.permissions.list) {
		//List of Categorie
		Ricerche.getList().then(function(data){
			$scope.ricerche = data;
		},function(error){});

		$rootScope.$on('Ricerche',function(event,data) {
			$scope.ricerche = data;
		});
		//code

		$scope.treeOptions = {
			accept: function(sourceNodeScope, destNodesScope, destIndex) {
				return true;
			},
			dropped: function(event) {
				var recurse = function (obj,parent) {
					var order = 0;
					_.each(obj, function (val, key){
						val.ordine = order;
						if (!parent) val.parent = val.parentId = null; //This level has no parent
						else val.parent = val.parentId = parent;
						if (val.nodes) {
							recurse(val.nodes,val.id)
						}
						order++;
					});
				};
				recurse($scope.ricerche);
				console.log ($scope.ricerche);
				Ricerche.updateOrder($scope.ricerche);
			}
		};
	}
})
.controller('RicercheEditCtrl', function($scope,$rootScope,Ricerche,$state,$http,$timeout) {
	console.log("RicercheEditCtrl");
	$scope.showInterface = false;
	//TOOLGRAPH
	$scope.editProfile = true;

	//TOOLGRAPH
	$scope.usr = new Object();
	if (Ricerche.getUser()) {
		//Modifica
		$scope.usr = Ricerche.getUser();
		//$scope.profiles = $scope.usr.profili;
		$scope.showInterface = true;
	} else {
		//Nuovo inserimento
		//Ricerche.getProfiles().then(function(data) {$scope.profiles = data;},function(error){});
		$scope.showInterface = true;
	}
})
.factory('Ricerche', function($q,$http,$rootScope) {
	var save = function(data) {
		var deferred = $q.defer();
		$http.post($rootScope.ws+'/ricerche/save',data).
		success(function(data, status, headers, config) {
			//Received configuration, setting global data
			deferred.resolve(data);
		}).
		error(function(data, status, headers, config) {
			// called asynchronously if an error occurs
			// or server returns response with an error status.
			deferred.reject(data);
		});
		return deferred.promise;
	};
	var getActive = function(data) {
		var deferred = $q.defer();
		if (localStorage.getItem('lastRicercheService')) {
			var a = moment();
			var b = localStorage.getItem('lastRicercheService');
			if (a.diff(b,'minutes') <= 1) {
				configurationDB.get('ricercheData', function(err, doc) {
				  if (err) { deferred.reject(err); return console.log(err); }
				  deferred.resolve(doc.data);
				});
				return deferred.promise;
			}
		}
		$http.post($rootScope.ws+'/ricerche/get-active',data).
		success(function(data, status, headers, config) {
			var dataDB = {};
			dataDB.data = data;
			localStorage.setItem('lastRicercheService',moment());
			dataDB._id =  'ricercheData';
			configurationDB.get('ricercheData', function(err, doc) {
				if (!err) {
					dataDB._rev = doc._rev;
				}
			    console.log(dataDB);
				configurationDB.put(dataDB, function(err, response) {
				if (err) { return console.log(err); }
				});
			});
			deferred.resolve(data);
		}).
		error(function(data, status, headers, config) {
			deferred.reject(false);
		});
		return deferred.promise;
	};
	var getList = function(data) {
		var deferred = $q.defer();
		$http.post($rootScope.ws+'/ricerche/get-list',data).
		success(function(data, status, headers, config) {
			deferred.resolve(data);
		}).
		error(function(data, status, headers, config) {
			deferred.reject(false);
		});
		return deferred.promise;
	};
	var publish = function(data,status) {
		data.active = status;
		var deferred = $q.defer();
		$http.post($rootScope.ws+'/ricerche/publish',data).
		success(function(data, status, headers, config) {
			//Received configuration, setting global data
			deferred.resolve(data);
		}).
		error(function(data, status, headers, config) {
			// called asynchronously if an error occurs
			// or server returns response with an error status.
			deferred.reject(data);
		});
		return deferred.promise;
	};
	var updateOrder = function(data) {
		var deferred = $q.defer();
		$http.post($rootScope.ws+'/ricerche/update-order',data).
		success(function(data, status, headers, config) {
			//Received configuration, setting global data
			deferred.resolve(data);
		}).
		error(function(data, status, headers, config) {
			// called asynchronously if an error occurs
			// or server returns response with an error status.
			deferred.reject(data);
		});
		return deferred.promise;
	};
	var del = function(data) {
		var deferred = $q.defer();
		$http.post($rootScope.ws+'/ricerche/delete',data).
		success(function(data, status, headers, config) {
			//Received configuration, setting global data
			deferred.resolve(data);
		}).
		error(function(data, status, headers, config) {
			// called asynchronously if an error occurs
			// or server returns response with an error status.
			deferred.reject(data);
		});
		return deferred.promise;
	};
	return {
		save : save,
		getActive : getActive,
		getList : getList,
		updateOrder : updateOrder,
		publish : publish,
		del : del
	};
})
