'use strict';
/* Controllers */
angular.module('cmsApp.users', [])
.controller('UsersCtrl', function($scope,$rootScope,Users,$state,moduleObj) {
    console.log(moduleObj);
	if (!_.isEmpty(moduleObj) && !_.isEmpty(moduleObj.p)) {
		$scope.permissions = moduleObj.p;
    console.log($scope.permissions);
		$scope.doInsert = function(data) {
			if ($scope.permissions.add) {
				Users.set(new Object());
				$state.go('logged.users.edit');
			}
		}
		$scope.doEdit = function(data) {
			if ($scope.permissions.edit) {
				console.log(data);
				Users.set(data);
				$state.go('logged.users.edit');
			}
		};
		$scope.doSave = function(data) {
			if ($scope.permissions.edit) Users.save(data).then(function(data){$state.go('logged.users.list');},function(error){});
		};
		$scope.doDelete = function(data) {
			if ($scope.permissions.delete)  {
				Users.del(data).then(function(data){
					$rootScope.$broadcast('Users',data);
				},function(error){});
			}
		}
	}
})
.controller('UsersListCtrl', function($scope,$rootScope,Users) {
	console.log("UsersListCtrl");
	if ($scope.permissions.list) {
		//List of Users
		Users.get().then(function(data){
			$scope.users = data;
		},function(error){});

		$rootScope.$on('Users',function(event,data) {
			$scope.users = data;
		});
		//code
	}
})
.controller('UsersUpdateCtrl', function($scope,$rootScope,Users,$state,usrObj,$timeout,$translate) {
	console.log("UsersUpdateCtrl");
	$scope.showInterface = false;
	$scope.editProfile = false;

	$scope.usr = new Object();
	$scope.usr.files = [];
	if (usrObj) {
		//Modifica
		console.log(usrObj);
		$scope.usr = usrObj;
		//$scope.profiles = $scope.usr.profili;
		$scope.showInterface = true;
		console.log($scope.usr);
	} else {
		console.log("nessuno user");
		$state.go('logged.main');
	}
})
.controller('UsersEditCtrl', function($scope,$rootScope,Users,$state,$http,$timeout) {
	console.log("UsersEditCtrl");
	$scope.showInterface = false;
	//TOOLGRAPH
	$scope.editProfile = true;

	//TOOLGRAPH
	$scope.usr = new Object();
	if (Users.getUser()) {
		//Modifica
		$scope.usr = Users.getUser();
		//$scope.profiles = $scope.usr.profili;
		$scope.showInterface = true;
	} else {
		//Nuovo inserimento
		//Users.getProfiles().then(function(data) {$scope.profiles = data;},function(error){});
		$scope.showInterface = true;
	}
})
.factory('Users', function($q,$http,$rootScope) {
	var obj = new Object();
	var get = function(data) {
		var deferred = $q.defer();
		$http.post($rootScope.ws+'/users/get-user',data).
		success(function(data, status, headers, config) {
			//Received configuration, setting global data
			deferred.resolve(data);
		}).
		error(function(data, status, headers, config) {
			// called asynchronously if an error occurs
			// or server returns response with an error status.
			deferred.reject(data);
		});
		return deferred.promise;
	};
	var set = function(data) {
		if (!data.files) {
			data.files = [];
		}
		obj = data;
		return true;
	};
	var getUser = function() {
		if (obj.idu) {
			return obj;
		}
		return false;
	};
	var getProfiles = function(data) {
		var deferred = $q.defer();
		$http.post($rootScope.ws+'/users/profiles').
		success(function(data, status, headers, config) {
			//Received configuration, setting global data
			deferred.resolve(data);
		}).
		error(function(data, status, headers, config) {
			// called asynchronously if an error occurs
			// or server returns response with an error status.
			deferred.reject(data);
		});
		return deferred.promise;
	};
	var save = function(data) {
		var deferred = $q.defer();
		$http.post($rootScope.ws+'/users/save',data).
		success(function(data, status, headers, config) {
			//Received configuration, setting global data
			deferred.resolve(data);
		}).
		error(function(data, status, headers, config) {
			// called asynchronously if an error occurs
			// or server returns response with an error status.
			deferred.reject(data);
		});
		return deferred.promise;
	};
	var del = function(data) {
		var deferred = $q.defer();
		$http.post($rootScope.ws+'/users/delete',data).
		success(function(data, status, headers, config) {
			//Received configuration, setting global data
			deferred.resolve(data);
		}).
		error(function(data, status, headers, config) {
			// called asynchronously if an error occurs
			// or server returns response with an error status.
			deferred.reject(data);
		});
		return deferred.promise;
	};
	return {
		get : get,
		set : set,
		getUser : getUser,
		getProfiles : getProfiles,
		save: save,
		del : del
	};
})
