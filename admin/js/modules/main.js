angular.module('cmsApp.mainCtrl', [])
.controller('MainCtrl', function($scope,$rootScope,filterFilter,Conf,Auth,Ricerche,Prodotti) {
	$scope.searchText = '';
	$scope.search = {};
	$scope.amazonTableControl = {};

	Conf.getConf().then(function(data){
		$rootScope.$broadcast('userConfLoaded',{'source':'getConf'});
	});

	if (localStorage.getItem('currentSearch')) {
		$scope.params = angular.fromJson(localStorage.getItem('currentSearch'));
	} else $scope.params = {'offset':0,'limit':20};

	console.log($scope.params);
	$scope.chooseClient = function(c) {
		if (confirm('Desideri consultare il catalogo per il cliente '+c.ragione_sociale+' ?')) {
			Auth.chooseClient(c).then(function(data){
				console.log(data);
				$scope.searchText = '';
				$rootScope.$broadcast('userConfChanged',{'source':'chooseClient'});
			},function(err){
				alert(err);
			});
		}
	}
	$scope.doSearch = function(params){
		console.log(params);
		delete($scope.params.filtersData);
		delete($scope.params.sortBy);
		$scope.params.filtersData = [];
		$scope.params.filtersData.push({'field':'prodotti.id','text':'Riferimento','type':'text','value':$scope.search.value})
		$scope.amazonTableControl.doSearch(true);
	}

    $scope.$watch('searchText', function() {
    	if ($scope.searchText.length > 3) {
    		$scope.filteredNames = filterFilter($rootScope.clientsList, $scope.searchText); // by text
    	}
	});
	Ricerche.getActive().then(function(data){
		console.log(data);
		$scope.ricerche = data;
	});

});
