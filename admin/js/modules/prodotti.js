'use strict';
/* Controllers */
angular.module('cmsApp.prodotti', [])
.controller('ProductDetailCtrl', function($scope,$rootScope,$state,$stateParams,Conf,Auth,Prodotti) {
	console.log($stateParams);
	$scope.qty = 1;

	if ($stateParams.id) {
		Prodotti.getDetail({'id':$stateParams.id}).then(function(data) {
			console.log(data);
			$scope.data = data;
		});
	}

	$scope.addCart = function(data) {
		if(data && angular.isNumber($scope.qty)) {
			if (confirm('Desideri inserire questo prodotto ?')) {
				Prodotti.addCart(data, $scope.qty).then(function(data){
					alert('aggiunto al carrello');
				},function(err){
					alert(err);
				});
			}
		}
	}
})
.factory('Prodotti', function($q,$http,$rootScope) {
	var getList = function(data) {
		var deferred = $q.defer();
		$http.post($rootScope.ws+'/products/get',data).
		success(function(data, status, headers, config) {
			deferred.resolve(data);
		}).
		error(function(data, status, headers, config) {
			deferred.reject(false);
		});
		return deferred.promise;
	};
	var getDetail = function(data) {
		var deferred = $q.defer();
		$http.post($rootScope.ws+'/products/details',data).
		success(function(data, status, headers, config) {
			deferred.resolve(data);
		}).
		error(function(data, status, headers, config) {
			deferred.reject(false);
		});
		return deferred.promise;
	};
	var getFilter = function(data) {
		var deferred = $q.defer();
		$http.post($rootScope.ws+'/products/get-filters',data).
		success(function(data, status, headers, config) {
			deferred.resolve(data);
		}).
		error(function(data, status, headers, config) {
			deferred.reject(false);
		});
		return deferred.promise;
	};
	var getCounter = function(data) {
		var deferred = $q.defer();
		$http.post($rootScope.ws+'/products/get-count',data).
		success(function(data, status, headers, config) {
			deferred.resolve(data);
		}).
		error(function(data, status, headers, config) {
			deferred.reject(false);
		});
		return deferred.promise;
	};

	/*============================= CARRELLO ===================================*/
	var getCart = function() {
		var params = {
			'id_user' : $rootScope.user.id,
			'id_cliente' : $rootScope.userConf.id_cliente,
		};

		console.log('get carrello', params)
		var deferred = $q.defer();
		$http.post($rootScope.ws+'/carrello/get',params).
		success(function(data, status, headers, config) {
			deferred.resolve(data);
		}).
		error(function(data, status, headers, config) {
			deferred.reject(false);
		});
		return deferred.promise;
	}

	var addCart = function(data, qty) {
		var params = {
      'id_user' : $rootScope.user.id,
      'id_cliente' : $rootScope.userConf.id_cliente,
      'id_prodotto' : data.id_prodotto,
      'quantita' : qty,
      'prezzo' : data.prezzo_listino,
      'deposito' : data.id_deposito
		};
		console.log('aggiunta carrello', params)
		var deferred = $q.defer();
		$http.post($rootScope.ws+'/carrello/save',params).
		success(function(data, status, headers, config) {
			deferred.resolve(data);
		}).
		error(function(data, status, headers, config) {
			deferred.reject(false);
		});
		return deferred.promise;
	};

	return {
		getList : getList,
		getFilter : getFilter,
		getCounter : getCounter,
		getDetail : getDetail,
		getCart : getCart,
		addCart : addCart
	};
})
