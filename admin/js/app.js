var configurationDB = new PouchDB("configuration");
angular.module('cmsApp',[
	'ui.bootstrap',
	'ui.router',
	'ui.utils',
	'ui.select',
	'ui.sortable',
	'ui.tree',
	'ngSanitize',
	'ngAnimate',
	'pascalprecht.translate',
	'translations',
	'cmsApp.controllers',
	'cmsApp.mainCtrl',
	'cmsApp.services',
	'cmsApp.users',
	'cmsApp.ricerche',
	'cmsApp.prodotti',
	'ngCkeditor',
	//'datatables'
	'angularUtils.directives.dirPagination'
	])
.run(function($rootScope,$state,Auth) {
	$rootScope.project = {};

	$rootScope.modules = [];
	$rootScope.project.Name = "Tacaje";
	$rootScope.project.Version = "0.1";
	$rootScope.frontendWs = '../ws';
	$rootScope.ws = 'ws';
	$rootScope.mainLanguage = 'it';
	$rootScope.$on('loggedin',function(event,data) {
		$state.go('logged.main');
	});
	$rootScope.$on('logout',function(event,data) {
		Auth.logout().then(function() {
			localStorage.clear();
			sessionStorage.clear();
			configurationDB.get('userConf', function(err, doc) {
			  if (err) { return console.log(err); }
			  configurationDB.remove(doc, function(err, response) {
			    if (err) { return console.log(err); }
			    // handle response
			  });
			});
			$rootScope.modules = [];
			$rootScope.permissions = [];
			$rootScope.clientsList = [];
			$rootScope.userConf = {};
			$state.go('login');
		});
	});
	$rootScope.doLogout = function() {
		$rootScope.$broadcast('logout');
	}
})
.config(function($stateProvider,$urlRouterProvider) {
	var authenticated = function ($q ,Auth,$state,$rootScope) {
		var deferred = $q.defer();
		Auth.isLoggedIn()
		.then(function (isLoggedIn) {
			deferred.resolve(true);
		}, function(error) {
			$rootScope.$broadcast('logout');
			localStorage.clear();
			sessionStorage.clear();
			deferred.reject(false);
		});
		return deferred.promise;
	};

	$stateProvider
	.state("login", {
		url: "/",
		templateUrl: 'pages/login.html',
		controller: 'LoginCtrl',
		resolve: { clearCache: function() { localStorage.clear(); }}
	})
	.state("logged", {
		url: "/logged",
		templateUrl: 'pages/logged.html',
		controller: 'LoggedCtrl',
		resolve: {authenticated: authenticated}
	})
	.state("logged.main", {
		url: "/main",
		templateUrl: 'pages/logged/main.html',
		controller: 'MainCtrl',
		resolve: {authenticated: authenticated}
	})
	/*
	 * USERS
	 */
	 .state("logged.users", {
	 	url: "/users",
	 	templateUrl: 'pages/logged/users/index.html',
	 	controller: 'UsersCtrl as pc',
	 	resolve: {
	 		authenticated: authenticated,
	 		moduleObj: function($q,Conf) {
	 			var module = 'user';
	 			var deferred = $q.defer();
	 			Conf.checkPermissions(module).then(function(data) {
	 				deferred.resolve(data);
	 			},function(error){
	 				deferred.reject(false);
	 			});
	 			return deferred.promise;
	 		}
	 	}
	 })
	 .state("logged.users.list", {
	 	url: "/list",
	 	templateUrl: 'pages/logged/users/list.html',
	 	controller: 'UsersListCtrl',
	 	resolve: {authenticated: authenticated}
	 })
	 .state("logged.users.edit", {
	 	url: "/edit",
	 	templateUrl: 'pages/logged/users/edit.html',
	 	controller: 'UsersEditCtrl',
	 	resolve: {authenticated: authenticated}
	 })
	 .state("logged.users.update", {
	 	url: "/update",
	 	templateUrl: 'pages/logged/users/update.html',
	 	controller: 'UsersUpdateCtrl',
	 	resolve: {
	 		usrObj : function(Users,$q,$rootScope,Auth) {
	 			var deferred = $q.defer();
	 			Auth.isLoggedIn()
	 			.then(function (isLoggedIn) {
	 				Users.get({id: $rootScope.user.idu}).then(function(data) {
	 					console.log(data);
	 					Users.set(data);
	 					deferred.resolve(data);
	 				});
	 			}, function(error) {
	 				deferred.reject('Not logged in');
	 			});
	 			return deferred.promise;
	 		}
	 	}
	 })
	/*
	 * USERS
	 */

	 /*
	 * RICERCHE
	 */
	 .state("logged.ricerche", {
	 	url: "/ricerche",
	 	templateUrl: 'pages/logged/ricerche/index.html',
	 	controller: 'RicercheCtrl',
	 	resolve: {
	 		authenticated: authenticated,
	 		moduleObj: function($q,Conf) {
	 			var module = 'ricerche';
	 			var deferred = $q.defer();
	 			Conf.checkPermissions(module).then(function(data) {
	 				deferred.resolve(data);
	 			},function(error){
	 				deferred.reject(false);
	 			});
	 			return deferred.promise;
	 		}
	 	}
	 })
	 .state("logged.ricerche.list", {
	 	url: "/list",
	 	templateUrl: 'pages/logged/ricerche/list.html',
	 	controller: 'RicercheListCtrl',
	 	resolve: {authenticated: authenticated}
	 })
	 .state("logged.ricerche.edit", {
	 	url: "/edit",
	 	templateUrl: 'pages/logged/ricerche/edit.html',
	 	controller: 'RicercheEditCtrl',
	 	resolve: {authenticated: authenticated}
	 })
	/*
	 * RICERCHE
	 */

	 /*
	 * PRODOTTI
	 */
	 .state("logged.productDetail", {
	 	url: "/productDetail/:id",
	 	templateUrl: 'pages/logged/prodotti/detail.html',
	 	controller: 'ProductDetailCtrl',
	 	resolve: {
	 		authenticated: authenticated
	 	}
	 })
	/*
	 * RICERCHE
	 */
;

$urlRouterProvider.otherwise('/');

})
.run(function ($rootScope, $state, $log) {
	$rootScope.$on('$stateChangeError', function(event, toState, toParams, fromState, fromParams, error) {
		event.preventDefault();
		throw(error);
	});
})
.filter('range', function() {
	return function(input, total) {
		total = parseInt(total);
		for (var i=0; i<total; i++)
			input.push(i);
		return input;
	};
})

.directive('ngConfirmClick', [
	function(){
		return {
			link: function (scope, element, attr) {
				var msg = attr.ngConfirmClick || "Are you sure?";
				var clickAction = attr.confirmedClick;
				element.bind('click',function (event) {
					if ( window.confirm(msg) ) {
						scope.$eval(clickAction)
					}
				});
			}
		};
	}]);
