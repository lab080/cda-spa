cmsApp
.directive('amazonTable', function($document,$timeout,$rootScope,$state,$stateParams,$anchorScroll,$location,Prodotti,Ricerche) {
	return {
	    restrict: "E",
	    replace: true,
	    templateUrl: 'tools/amazon-table.html',
	    scope: {
	        label: '@',
	        id: '@',
	        params: '=params',
	        control: '=',
	    },
	    link: function($scope,element,attrs) {
	    	$scope.$on('userConfChanged',function(event,data) {
				console.log('userConfChanged');
				console.log(data);
				console.log($rootScope.user);
				console.log($rootScope.userConf);
				if ($rootScope.userConf) {
					$scope.control.doSearch(true);
				}
				$scope.pagination = {
		       		current: 1
		    	};
				if ($rootScope.user.id_profilo == 2 || $rootScope.id_profilo == 100) $scope.isAdmin = true;
			});
	    	$scope.$on('userConfLoaded',function(event,data) {
				console.log('userConfLoaded');
				if ($rootScope.userConf) {
					$scope.control.doSearch(false);
				}
				if ($rootScope.user.id_profilo == 2 || $rootScope.id_profilo == 100) $scope.isAdmin = true;
			});
			$scope.isAdmin = false;
			$scope.saveSearchConfirm = false;
        	$scope.filtersData = []; //FIltri di ricerca
        	$scope.limit = 10; //FIltri di ricerca
        	$scope.range = 10; //FIltri di ricerca
        	$scope.totalItems = 0;
    		$scope.pagination = {
		        current: 1
		    };
			$scope.dataPerPage = $scope.params.limit;
			$scope.pageChanged = function(newPage) {
		        getResultsPage(newPage);
		    };

		    $scope.filters = {'fields':[]};
			$scope.filters.fields = [	{'field':'prodotti.id','text':'Riferimento','type':'text'},
										{'field':'prodotti.listino','text':'Listino','type':'text'},
										{'field':'categorie_data.id_categoria','text':'Gruppo','type':'list'},
										{'field':'prodotti.id_catalogo','text':'Marca','type':'list'},
										{'field':'prodotti_cross.id_catalogo','text':'Catalogo','type':'list'},
										{'field':'prodotti.new','text':'Nuovo','type':'boolean'},
			];
			$scope.sortBy=[
				{'field':'prodotti.text','text':'Prodotto','sort':'asc'},
				{'field':'prodotti.id_categoria','text':'Categoria','sort':'asc'},
				{'field':'prodotti.id_catalogo','text':'Catalogo','sort':'asc'},
				{'field':'prodotti_giacenze.stato','text':'Stato','sort':'asc'},
				{'field':'prodotti.listino','text':'Listino:crescente','sort':'asc'},
				{'field':'prodotti.listino','text':'Listino:decrescente','sort':'desc'},
				{'field':'prodotti_listino.prezzo','text':'Netto:crescente','sort':'asc'},
				{'field':'prodotti_listino.prezzo','text':'Netto:decrescente','sort':'desc'}
			]
			$scope.control.doSearch = function(force) {
				if (force) clearStorage();
				if (localStorage.getItem('currentSearch')) {
					$scope.params = angular.fromJson(localStorage.getItem('currentSearch'));
					//$scope.sortBy = angular.fromJson($scope.params.sortBy);
					console.log($scope.params);
				}
				if (localStorage.getItem('totalItems')) {
					$scope.totalItems = angular.fromJson(localStorage.getItem('totalItems'));
				}
				if (localStorage.getItem('currentFilters')) {
					$scope.filters = angular.fromJson(localStorage.getItem('currentFilters'));
					console.log($scope.filters);
				} else {
					Prodotti.getFilter($scope.params).then(function (data){
						$scope.totalItems = data.values.data;
						localStorage.setItem('totalItems',angular.toJson($scope.totalItems));
						if (data.values.categorie) {
							var index = _.findIndex($scope.filters.fields, {'field': 'categorie_data.id_categoria'});
							if (index >= 0) {
								$scope.filters.fields[index].values = data.values.categorie;
							}
						}
						if (data.values.marche) {
							var index = _.findIndex($scope.filters.fields, {'field': 'prodotti.id_catalogo'});
							if (index >= 0) {
								$scope.filters.fields[index].values = data.values.marche;
							}
						}
						if (data.values.cataloghi) {
							var index = _.findIndex($scope.filters.fields, {'field': 'prodotti_cross.id_catalogo'});
							if (index >= 0) {
								$scope.filters.fields[index].values = data.values.cataloghi;
							}
						}
						console.log($scope.filters);
						localStorage.setItem('currentFilters',angular.toJson($scope.filters));
					});
				}
				if (localStorage.getItem('currentPage')) {
					var page = angular.fromJson(localStorage.getItem('currentPage'));
					$scope.pagination = {
				        current: page
				    };
					getResultsPage(page);
				}
				else getResultsPage(1);
			};
			function clearStorage() {
				console.log('clearStorage');
				localStorage.removeItem('currentSearch');
				localStorage.removeItem('currentPage');
				localStorage.removeItem('totalItems');
				localStorage.removeItem('currentFilters');
			}
		    function getResultsPage(pageNumber) {
		    	$scope.params.offset = $scope.params.limit * (pageNumber-1);
		    	localStorage.setItem('currentSearch',angular.toJson($scope.params));
		    	localStorage.setItem('currentPage',pageNumber);
		        Prodotti.getList($scope.params).then(function (data){
					$scope.data = data.values;
				});
		    }

		    $scope.applyOrder = function() {
		    	$scope.params.sortBy = angular.fromJson($scope.sortBy.selected);
		    	getResultsPage(1);
		    }

        	$scope.applyFilters = function(){
        		$location.hash('top');
        		$anchorScroll();
        		$scope.params.filtersData = [];
        		angular.forEach($scope.filters.fields, function(field, key) {
        			switch (field.type) {
        				case 'list':
        					angular.forEach(field.values, function(value, key) {
								if (value.checked) {
									console.log(value);
									var obj = {field:field.field,value:value};
									$scope.params.filtersData.push(obj);
								}
							});
        				break;
        				case 'text':
        					if (field.value) {
								console.log(field);
								var obj = {field:field.field,value:field.value};
								$scope.params.filtersData.push(obj);
							}
        				break;
        				case 'boolean':
        					if (field.checked) {
								console.log(field);
								var obj = {field:field.field,value:field.value};
								$scope.params.filtersData.push(obj);
							}
        				break;
        			}
				});
				Prodotti.getCounter($scope.params).then(function (data){
					$scope.totalItems = data.count;
					localStorage.setItem('totalItems',angular.toJson($scope.totalItems));
				});
				getResultsPage(1);
				localStorage.setItem('currentFilters',angular.toJson($scope.filters));
				localStorage.setItem('currentSearch',angular.toJson($scope.params));
        	}; //FIltri di ricerca

        	$scope.saveSearchAs = function() {
        		if (confirm("Vuoi salvare la ricerca corrente?")) {
	        		$scope.customSearch.params = $scope.params;
	        		delete $scope.params.offset;
	        		delete $scope.params.limit;
	        		Ricerche.save($scope.customSearch).then(function(){ 
	        			$scope.saveSearchConfirm = true;
	        			$timeout(function () { $scope.saveSearchConfirm = false; }, 3000);
	        		});
	        	}
        	}

			
	    }
	};
});