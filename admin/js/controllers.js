'use strict';
/* Controllers */
var cmsApp = angular.module('cmsApp.controllers', [])
.controller('LoginCtrl', function($scope,$rootScope,Login,Conf) {
	console.log("LoginCtrl");
	$scope.hasError = false;
	$scope.pwdForgot = false;


	$scope.doLogin = function() {
		$scope.hasError = false;
		Login.doLogin({'u':$scope.user.username,'p':$scope.user.password})
		.then(function(data) {
			$rootScope.$broadcast('loggedin');
		},
		function(error){
			$scope.hasError = true;
		});
	}

	$scope.doPwdReset = function() {
		$scope.hasError = false;
		Login.doPwdReset({'u':$scope.user.username})
		.then(function(data) {
			//$rootScope.$broadcast('loggedin');
			$scope.pwdSent = true;
		},
		function(error){
			$scope.hasError = true;
		});
	}
})
.controller('LoggedCtrl', function($scope,$rootScope,$state,Conf,Users,Ricerche,Prodotti) {
	$scope.cartList = false;

	$scope.editMyUser = function() {
		$state.go('logged.users.update');
	}
	Conf.getConf().then(function(data){
		console.log($rootScope.permissions);
	});

	$scope.getCart = function() {
		Prodotti.getCart().then(function(data){
			$scope.cartList = false;
			if(data.length > 0) {
				$scope.cartList = data;
			}
		},function(err){
			alert(err);
		});
	}
});
