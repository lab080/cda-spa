'use strict';
/* Services */
angular.module('cmsApp.services', [])
.value('version', '0.1')
.factory('Auth', function($q,$http,$rootScope,$state,Conf) {
	var isLoggedIn = function() {
		var deferred = $q.defer();
		$http.post($rootScope.ws+'/auth/check',{'token':sessionStorage.getItem('tokenAdmin')}).
		success(function(data, status, headers, config) {
			Conf.getConf().then(
				function(){
					deferred.resolve(true);
				},
				function(){
					localStorage.clear();
					sessionStorage.clear();
					$rootScope.$broadcast('logout');
					$state.go('login');
					deferred.reject(data);	
				}
			);
			
		})
		.error(function(data, status, headers, config) {
			localStorage.clear();
			sessionStorage.clear();
			$rootScope.$broadcast('logout');
			$state.go('login');
			deferred.reject(data);
		});
		return deferred.promise;
	};
	var logout = function() {
		var deferred = $q.defer();
		$http.post($rootScope.ws+'/auth/logout').
		success(function(data, status, headers, config) {
			deferred.resolve(data);
		})
		.error(function(data, status, headers, config) {
			deferred.reject(data);
		});
		return deferred.promise;
	};
	var chooseClient = function(c) {
		var deferred = $q.defer();
		$http.post($rootScope.ws+'/auth/choose-client',{'c':c.id}).
		success(function(data, status, headers, config) {
			$rootScope.userConf = data;
			var dataDb = {}
			dataDb.userConf = data;
			dataDb._id = 'userConf';
			configurationDB.get('userConf', function(err, doc) {
			  	if (!err) {
			  		dataDb._rev = doc._rev;
			  		if (doc.user) dataDb.user = doc.user;
			  		if (doc.clientsList) dataDb.clientsList = doc.clientsList;
			  		if (doc.permissions) dataDb.permissions = doc.permissions; 
			  	}
				configurationDB.put(dataDb, function(err, response) {
				if (err) { return console.log(err); }
				});
			});

			deferred.resolve(data);
		})
		.error(function(data, status, headers, config) {
			deferred.reject(data);
		});
		return deferred.promise;
	}

  	return {
		isLoggedIn : isLoggedIn,
		logout:logout,
		chooseClient:chooseClient
	};
})
.factory('Conf', function($q,$http,$rootScope) {
	var getConf = function() {
		var deferred = $q.defer();
		configurationDB.get('userConf', function(err, doc) { 
			if (err) { return deferred.reject(err); } 
			else { 
				if (doc.user) $rootScope.user = doc.user; 
				if (doc.userConf) $rootScope.userConf = doc.userConf; 
				if (doc.clientsList) $rootScope.clientsList = doc.clientsList; 
				if (doc.permissions) $rootScope.permissions = doc.permissions; 
				deferred.resolve(doc); 
			}
		});
		return deferred.promise;
	};
	var checkPermissions = function(module) {
		var deferred = $q.defer();
		configurationDB.get('userConf', function(err, doc) { 
			if (err) { return deferred.reject(data); } 
			else {
				deferred.resolve(findDeep(doc.permissions,{'modulo':module}));
			}
		});
		
		return deferred.promise;
	};

	return {
		getConf : getConf,
		checkPermissions : checkPermissions,
	};
})
.factory('Login', function($q,$http,$rootScope) {
	var doLogin = function(data) {
		var deferred = $q.defer();
		$http.post($rootScope.ws+'/auth/login',data).
		success(function(data, status, headers, config) {
			//Received configuration, setting global data
			sessionStorage.setItem('tokenAdmin', data.token);
			var dataDb = {}
			if (data.user) dataDb.user = data.user;
			if (data.user_conf) dataDb.userConf = data.user_conf;
			if (data.clients_list) dataDb.clientsList = data.clients_list;
			if (data.permissions) dataDb.permissions = data.permissions;
			dataDb._id = 'userConf';
			configurationDB.get('userConf', function(err, doc) {
			  	if (!err) dataDb._rev = doc._rev;
				configurationDB.put(dataDb, function(err, response) {
				if (err) { return console.log(err); }
				});
			});
			deferred.resolve(data);
		}).
		error(function(data, status, headers, config) {
			// called asynchronously if an error occurs
			// or server returns response with an error status.
			deferred.reject(data);
		});
		return deferred.promise;
	};
	var doPwdReset = function(data) {
		var deferred = $q.defer();
		$http.post($rootScope.ws+'/auth/forgot-password',data).
		success(function(data, status, headers, config) {
			deferred.resolve(true);
		}).
		error(function(data, status, headers, config) {
			deferred.reject(false);
		});
		return deferred.promise;
	};
	return {
		doLogin : doLogin,
		doPwdReset : doPwdReset
	};
});


function findDeep (items, attrs) {
  function match(value) {
    for (var key in attrs) {
      if(!_.isUndefined(value) && !_.isNull(value)) {
        if (attrs[key] !== value[key]) {
          return false;
        }
      }
    }

    return true;
  }

  function traverse(value) {

    	
    var result;
    var found = false;

    _.forEach(value, function (val) {
      if (match(val)) {
        result = val;
				if (!_.isNull(val)) found = true;
        return false;
      }

      if (!found && (_.isObject(val) || _.isArray(val))) {
        result = traverse(val);
      }

      if (_.isUndefined(result) && _.isNull(result)) {
        return false;
      }
				return false;
    });
    return result;
  }

  return traverse(items);

}
